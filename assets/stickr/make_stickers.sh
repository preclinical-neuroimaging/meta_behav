
convert dplyr.png -resize 300 dplyr.png
convert easystats.png -resize 300 easystats.png
convert forcats.png -resize 300 forcats.png
convert ggplot2.png -resize 300 ggplot2.png
convert knitr.png -resize 300 knitr.png
convert lubridate.png -resize 300 lubridate.png
convert parameters.png -resize 300 parameters.png
convert performance.png -resize 300 performance.png
convert rmarkdown.png -resize 300 rmarkdown.png
convert stickr.png -resize 300 stickr.png
convert stringr.png -resize 300 stringr.png

convert rmarkdown.png knitr.png ggplot2.png dplyr.png stringr.png +append -gravity West -background "#FFFFFF" logo_top.png

convert lubridate.png forcats.png easystats.png parameters.png performance.png stickr.png +append -gravity West -background "#FFFFFF" logo_bottom.png

convert logo_top.png logo_bottom.png -append -background "#FFFFFF" logo.png


pngappend logo_top.png - logo_bottom.png -background "#FFFFFF" logo.png
