Mouse meta behaviour
================

## Loading relevant libraries

Output session information to allow for replication.

``` r
source('assets/functions/load_env.R')
```

## Generate synthetic data

In the section below, we generate synthetic data. First, we split the
table as a function of experimental parameter. We also need to select
covariate for the MAIN+CoVariate and SECONDAY analysis. Unfortunately,
the list of covariate is restricted by data availability. Covariates are
selected to retain \~50% of the data entries.

The summary statistics and effect sizes are directly outputed into
tables in `assets/table` at the end of the `perm_summary` function for
later retrieval. Because this chunk is computationally heavy, it isn’t
run by default in the code.

Note, this will take a long long time to run with 10’000 permutations.
:-P I’ve now implemented parallel processing to speed up the process.

``` r
#filter out main data.frame 
df <- read_rds('assets/table/Animal_behaviour_preprocessed.rds.gz') %>% filter(.$Sex != 'Mixed sex', .$Mouse_strain != 'Other', !.$Age_in_months %in% c('Mixed age','Unknown'))

#full list of potential covariates
covariate.list<- c("Test_duration_total_min", "Luminosity_lux","Water_temperature_celsius"   ,"Humidity"  ,"Color_of_arena","Shape_of_arena" , "Arena_dimensions","Noise_level_dB","Time_of_day", "Light.dark_cycle","Housing_conditions", "Habituation_period" , "Test_frequency","Handling_duration_min(period_immediately_prior_to_testing)","Saline.injection")


##OF: Time spent in center (%)
exp.type <- 'OF: Time spent in center (%)'
exp.type.short<- 'OF_time_center'

df.sub <- df %>% filter(Experiment.parameter == exp.type) %>% 
                    mutate_if(is.factor,fct_drop)
#levels(df.sub$Sex)
#levels(df.sub$Mouse_strain)
#levels(df.sub$Age_bin)
# 
#summary(df.sub[,names(df.sub) %in% covariate.list])
covariates<-c('Test_duration_total_min','Housing_conditions', 'Light.dark_cycle')

df.sub[,names(df.sub)%in% covariates] %>%   complete.cases() %>%   sum() %>% {glue('Remaining number of entries {.} / {nrow(df.sub)}')}
#[1] "Remaining number of entries 267 / 404"

#takes ~30 seconds with 8 core parallel processing for 200 permutations. 
perm_summary(exp.type.short, df.sub,permutation, covariates)


##OF: Distance travelled (m/min)
exp.type <- 'OF: Distance travelled (m/min)'
exp.type.short<- 'OF_distance'

df.sub <- df %>% filter(Experiment.parameter == exp.type) %>% 
                    mutate_if(is.factor,fct_drop)
#levels(df.sub$Sex)
#levels(df.sub$Mouse_strain)
#levels(df.sub$Age_bin)
# 
#summary(df.sub[,names(df.sub) %in% covariate.list])
covariates<-c('Test_duration_total_min','Housing_conditions', 'Light.dark_cycle')

df.sub[,names(df.sub)%in% covariates] %>%   complete.cases() %>%   sum() %>% {glue('Remaining number of entries {.} / {nrow(df.sub)}')}
#[1] "Remaining number of entries 273 / 463"

perm_summary(exp.type.short, df.sub,permutation, covariates)


##EPM: Time spent in closed arms (%)
exp.type <- "EPM: Time spent in closed arms (%)"
exp.type.short<- 'EPM_time_closed'

df.sub <- df %>% filter(Experiment.parameter == exp.type) %>% 
                    mutate_if(is.factor,fct_drop)
# levels(df.sub$Sex)
# levels(df.sub$Mouse_strain)
# levels(df.sub$Age_bin)
# 
# summary(df.sub[,names(df.sub) %in% covariate.list])
covariates<-c('Test_duration_total_min','Housing_conditions', 'Light.dark_cycle')

df.sub[,names(df.sub)%in% covariates] %>%   complete.cases() %>%   sum() %>% {glue('Remaining number of entries {.} / {nrow(df.sub)}')}
#[1] "Remaining number of entries 172 / 269"

perm_summary(exp.type.short, df.sub,permutation, covariates)


##EPM: Time spent in open arms (%)
exp.type <- "EPM: Time spent in open arms (%)"
exp.type.short<- 'EPM_time_open'

df.sub <- df %>% filter(Experiment.parameter == exp.type) %>% 
                    mutate_if(is.factor,fct_drop)
# levels(df.sub$Sex)
# levels(df.sub$Mouse_strain)
# levels(df.sub$Age_bin)
# 
# summary(df.sub[,names(df.sub) %in% covariate.list])
covariates<-c('Test_duration_total_min','Housing_conditions', 'Light.dark_cycle')

df.sub[,names(df.sub)%in% covariates] %>%   complete.cases() %>%   sum() %>% {glue('Remaining number of entries {.} / {nrow(df.sub)}')}
#[1] "Remaining number of entries 222 / 351"

perm_summary(exp.type.short, df.sub,permutation, covariates)


##FST: Immobility time (%)
exp.type <- "FST: Immobility time (%)"
exp.type.short<- 'FST_immobility'

df.sub <- df %>% filter(Experiment.parameter == exp.type) %>% 
                    mutate_if(is.factor,fct_drop)
# levels(df.sub$Sex)
# levels(df.sub$Mouse_strain)
# levels(df.sub$Age_bin)
# 
# summary(df.sub[,names(df.sub) %in% covariate.list])
covariates<-c('Test_duration_total_min', 'Housing_conditions',  'Light.dark_cycle')

df.sub[,names(df.sub)%in% covariates] %>%   complete.cases() %>%   sum() %>% {glue('Remaining number of entries {.} / {nrow(df.sub)}')}
#[1] "Remaining number of entries 137 / 266"

perm_summary(exp.type.short, df.sub,permutation, covariates)
```
