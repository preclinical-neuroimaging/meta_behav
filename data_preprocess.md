Mouse meta behaviour
================

## Loading relevant libraries

Output session information to allow for replication.

``` r
source('assets/functions/load_env.R')
```

``` r
#output session info
sessionInfo()
```

    ## R version 4.0.4 (2021-02-15)
    ## Platform: x86_64-pc-linux-gnu (64-bit)
    ## Running under: Pop!_OS 21.10
    ## 
    ## Matrix products: default
    ## BLAS:   /usr/lib/x86_64-linux-gnu/blas/libblas.so.3.9.0
    ## LAPACK: /usr/lib/x86_64-linux-gnu/lapack/liblapack.so.3.9.0
    ## 
    ## locale:
    ##  [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
    ##  [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
    ##  [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
    ##  [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
    ##  [9] LC_ADDRESS=C               LC_TELEPHONE=C            
    ## [11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       
    ## 
    ## attached base packages:
    ## [1] parallel  stats     graphics  grDevices utils     datasets  methods  
    ## [8] base     
    ## 
    ## other attached packages:
    ##  [1] wesanderson_0.3.6 ggpubr_0.4.0      performance_0.8.0 effectsize_0.5   
    ##  [5] parameters_0.15.0 multcomp_1.4-17   TH.data_1.1-0     MASS_7.3-53.1    
    ##  [9] survival_3.2-10   mvtnorm_1.1-3     lme4_1.1-27.1     Matrix_1.3-2     
    ## [13] knitr_1.36        glue_1.6.1        lubridate_1.8.0   forcats_0.5.1    
    ## [17] stringr_1.4.0     dplyr_1.0.7       purrr_0.3.4       readr_2.1.0      
    ## [21] tidyr_1.1.4       tibble_3.1.6      ggplot2_3.3.5     tidyverse_1.3.1  
    ## 
    ## loaded via a namespace (and not attached):
    ##  [1] nlme_3.1-152      fs_1.5.0          insight_0.14.5    httr_1.4.2       
    ##  [5] tools_4.0.4       backports_1.4.0   utf8_1.2.2        R6_2.5.1         
    ##  [9] DBI_1.1.1         colorspace_2.0-2  withr_2.4.3       tidyselect_1.1.1 
    ## [13] compiler_4.0.4    cli_3.1.1         rvest_1.0.2       xml2_1.3.2       
    ## [17] sandwich_3.0-1    bayestestR_0.11.5 scales_1.1.1      digest_0.6.29    
    ## [21] minqa_1.2.4       rmarkdown_2.11    pkgconfig_2.0.3   htmltools_0.5.2  
    ## [25] dbplyr_2.1.1      fastmap_1.1.0     rlang_0.4.12      readxl_1.3.1     
    ## [29] rstudioapi_0.13   generics_0.1.1    zoo_1.8-9         jsonlite_1.7.2   
    ## [33] car_3.0-12        magrittr_2.0.1    Rcpp_1.0.8        munsell_0.5.0    
    ## [37] fansi_1.0.2       abind_1.4-5       lifecycle_1.0.1   stringi_1.7.6    
    ## [41] yaml_2.2.1        carData_3.0-4     grid_4.0.4        crayon_1.4.2     
    ## [45] lattice_0.20-41   haven_2.4.3       splines_4.0.4     hms_1.1.1        
    ## [49] pillar_1.6.4      boot_1.3-27       ggsignif_0.6.3    codetools_0.2-18 
    ## [53] reprex_2.0.1      evaluate_0.14     modelr_0.1.8      vctrs_0.3.8      
    ## [57] nloptr_1.2.2.3    tzdb_0.2.0        cellranger_1.1.0  gtable_0.3.0     
    ## [61] assertthat_0.2.1  datawizard_0.2.1  xfun_0.28         broom_0.7.10     
    ## [65] rstatix_0.7.0     ellipsis_0.3.2

## Load and examine table

Here we load the table, reformat the columns, and reorder the factors
for consistancy. Output table summary for QA/QC.

``` r
# load the table
df <-
  read_tsv('assets/table/Animal_behaviour_summary_stats.tsv',
           col_types = cols()) %>%
  
  #rename column names
  rename(Experiment.parameter = `Experiment+parameter`) %>%
  rename(Humidity = `Humidity%`) %>%
  rename(Arena_dimensions = `Arena_dimensions(mxm(+height)_or_diameter)`) %>%
  rename(Light.dark_cycle = `Light/dark_cycle`) %>%
  rename(Habituation_period =
           `Habituation_period(min_in_arena_before_start_of_test)`) %>%
  rename(Test_frequency =
           `Test_frequency(amount_of_times_test_is_repeated_to_get_to_summary_statistics)`) %>%
  rename(Saline.injection = `Saline injection`) %>%
  
  #rearrange Age_in_months
  mutate(Age_in_months = factor(Age_in_months)) %>%
  mutate(Age_in_months = fct_expand(Age_in_months, '>10')) %>%
  mutate(Age_in_months = replace(
    Age_in_months,
    Age_in_months %in% c('10-12', '12-14', '14-16', '16-18', '18-20', '20-22', '22-24'),
    '>10'
  )) %>%
  mutate(Age_in_months = fct_drop(Age_in_months)) %>%
  mutate(
    Age_in_months = fct_relevel(
      Age_in_months,
      '0-2',
      '2-4',
      '4-6',
      '6-8',
      '8-10',
      '>10',
      'Mixed age',
      'Unknown'
    )
  ) %>%
  mutate(Age_bin = factor(ifelse(
    Age_in_months %in% c('0-2', '2-4'), 'young', 'old'
  ))) %>%
  mutate(Age_bin = replace(Age_bin, Age_in_months %in% c('Mixed age', 'Unknown'), NA)) %>%
  
  #rearrange other factorial columns
  mutate(Sex = factor(Sex, levels = c("Female", "Male", "Mixed sex"))) %>%
  mutate(PMID = factor(PMID)) %>%
  mutate(Mouse_strain = factor(
    Mouse_strain,
    levels = c(
      "C57BL/6J",
      "BALB/cJ",
      "C57BL/6N",
      "DBA/2J",
      "129S1/SvImJ",
      "129X1/SvJ",
      "A/J",
      "AKR/J",
      "BALB/cByJ",
      "C3H/He",
      "C3H/HeJ",
      "C57BL/10J",
      "C57BL/6NTac",
      "C57BR",
      "CBA/J",
      "DBA/1J",
      "DBA/N",
      "FVB/N",
      "SJL",
      "Other"
    )
  )) %>%
  
  mutate(Experiment.parameter = factor(Experiment.parameter)) %>%
  mutate(Entry = factor(Entry)) %>%
  mutate(Summary_statistics = factor(Summary_statistics)) %>%
  mutate(Time_of_day = factor(Time_of_day)) %>%
  mutate(Light.dark_cycle = factor(Light.dark_cycle)) %>%
  mutate(Housing_conditions = factor(Housing_conditions)) %>%
  mutate(Saline.injection = factor(Saline.injection)) %>%
  mutate(Timestamp = mdy_hms(Timestamp, tz = 'CET')) %>%
  mutate(Luminosity_lux = as.numeric(Luminosity_lux))


# change mouse strain to other everything that isn't C57BL/6J, DBA/2J, BALB/cJ, or C57BL/6N
# make a dplyr/forcat conversion if possible
levels(df$Mouse_strain) <- c(levels(df$Mouse_strain), "Other")
df$Mouse_strain[!df$Mouse_strain %in% c('C57BL/6J', 'DBA/2J', 'C57BL/6N','BALB/cJ')]<-'Other'

# change age to >10 for every age group 10 months or older. 
# make a dplyr/forcat conversion if possible
levels(df$Age_in_months) <- c(levels(df$Age_in_months), ">10")
df$Age_in_months[df$Age_in_months %in% c("10-12","12-14","14-16","16-18","18-20","20-22","22-24")]<-'>10'


# table summary
summary(df) %>% kable("pipe")
```

|     | Timestamp                   | PMID          | Sample_size   | Sex            | Age_in_months | Mouse_strain   | Experiment.parameter                   | Test_duration_total_min | Luminosity_lux | Water_temperature_celsius | Humidity      | Color_of_arena   | Shape_of_arena   | Entry       | Summary_statistics     | Parameter_data   | Arena_dimensions | Noise_level_dB | Time_of_day                | Light.dark_cycle    | Housing_conditions | Habituation_period | Test_frequency | Handling_duration_min(period_immediately_prior_to_testing) | Saline.injection | Age_bin    |
|:----|:----------------------------|:--------------|:--------------|:---------------|:--------------|:---------------|:---------------------------------------|:------------------------|:---------------|:--------------------------|:--------------|:-----------------|:-----------------|:------------|:-----------------------|:-----------------|:-----------------|:---------------|:---------------------------|:--------------------|:-------------------|:-------------------|:---------------|:-----------------------------------------------------------|:-----------------|:-----------|
|     | Min. :2021-03-03 14:25:08   | 23288504: 59  | Min. : 3.00   | Female : 449   | 2-4 :1549     | C57BL/6J :1759 | EPM: Distance travelled (m/min) :106   | Min. : 1.000            | Min. : 0.0     | Min. : 0.00               | Min. :40.00   | Length:2520      | Length:2520      | Hannah:1379 | Boxplot \[x,y,z\]: 45  | Length:2520      | Length:2520      | Min. :40.00    | Afternoon (12:00-0:00: 66  | Inverted (D-L): 180 | Group :1237        | Min. : 0.50        | Min. :2.000    | Min. : 0.70                                                | No : 4           | old : 410  |
|     | 1st Qu.:2021-03-18 13:48:43 | 22028789: 37  | 1st Qu.: 9.00 | Male :1807     | 0-2 : 325     | BALB/cJ : 346  | EPM: Time spent in closed arms (%):364 | 1st Qu.: 5.000          | 1st Qu.: 40.0  | 1st Qu.:24.00             | 1st Qu.:45.00 | Class :character | Class :character | Sareen:1141 | Mean+SD \[x,y\] : 121  | Class :character | Class :character | 1st Qu.:55.00  | Morning (0:00-12:00) : 160 | Normal (L-D) :1958  | Single: 350        | 1st Qu.: 2.00      | 1st Qu.:2.000  | 1st Qu.: 30.00                                             | Yes : 363        | young:1874 |
|     | Median :2021-05-03 16:38:10 | 27647655: 32  | Median :11.00 | Mixed sex: 264 | 4-6 : 173     | Other : 213    | EPM: Time spent in open arms (%) :484  | Median : 5.000          | Median : 100.0 | Median :25.00             | Median :51.00 | Mode :character  | Mode :character  |             | Mean+SEM \[x,y\] :2354 | Mode :character  | Mode :character  | Median :55.00  | NA’s :2294                 | NA’s : 382          | NA’s : 933         | Median : 2.00      | Median :3.000  | Median : 60.00                                             | NA’s:2153        | NA’s : 236 |
|     | Mean :2021-06-01 23:27:55   | 18594797: 30  | Mean :12.32   |                | Unknown : 137 | C57BL/6N : 117 | FST: Immobility time (%) :367          | Mean : 9.385            | Mean : 190.1   | Mean :24.44               | Mean :53.12   |                  |                  |             |                        |                  |                  | Mean :59.78    |                            |                     |                    | Mean : 7.07        | Mean :2.878    | Mean : 46.19                                               |                  |            |
|     | 3rd Qu.:2021-08-03 12:28:51 | 19931548: 29  | 3rd Qu.:14.00 |                | 6-8 : 111     | DBA/2J : 85    | FST: Time spent swimming (%) : 34      | 3rd Qu.: 10.000         | 3rd Qu.: 200.0 | 3rd Qu.:25.00             | 3rd Qu.:60.00 |                  |                  |             |                        |                  |                  | 3rd Qu.:65.00  |                            |                     |                    | 3rd Qu.:10.00      | 3rd Qu.:3.000  | 3rd Qu.: 60.00                                             |                  |            |
|     | Max. :2021-12-17 21:36:18   | 28104556: 24  | Max. :63.00   |                | Mixed age: 99 | 129S1/SvImJ: 0 | OF: Distance travelled (m/min) :613    | Max. :120.000           | Max. :1600.0   | Max. :35.00               | Max. :70.00   |                  |                  |             |                        |                  |                  | Max. :77.00    |                            |                     |                    | Max. :60.00        | Max. :6.000    | Max. :120.00                                               |                  |            |
|     |                             | (Other) :2309 |               |                | (Other) : 126 | (Other) : 0    | OF: Time spent in center (%) :552      | NA’s :25                | NA’s :1735     | NA’s :2184                | NA’s :2126    |                  |                  |             |                        |                  |                  | NA’s :2448     |                            |                     |                    | NA’s :2360         | NA’s :2471     | NA’s :2347                                                 |                  |            |

## Convert summary statistics to mean and SD

At the moment, the database only contains Mean+SEM, Mean+SD, or boxplot
data. See [pre-registration](https://osf.io/8avnb) for other
transformations if necessary.

``` r
df$mean <- c()
df$sd <- c()

# for every row in df
for(iter in 1:dim(df)[1]){
  #split the parameters, there should be two entries for mean+SEM and Mean+Sd and three entries from boxplot. 
  #Ive added a check for parameter length with warning message if this isn't the case. 
parameter_split <- as.numeric(unlist(strsplit(as.character(df$Parameter_data[iter]),', ')))
  #case Mean+SEM
  if (df$Summary_statistics[iter] == 'Mean+SEM [x,y]'){
    if(length(parameter_split)!=2){print(glue('Warning: entrie {iter}does not contain the correct number of parameters'));next}
    df$mean[iter]<-parameter_split[1]
    df$sd[iter]<-parameter_split[2] * sqrt(df$Sample_size[iter])
    
  #case Mean+SD
  }else if (df$Summary_statistics[iter] == 'Mean+SD [x,y]'){
    if(length(parameter_split)!=2){print(glue('Warning: entrie {iter}does not contain the correct number of parameters'));next}
    df$mean[iter]<-parameter_split[1]
    df$sd[iter]<-parameter_split[2]
    
  #case Boxplot
  }else if (df$Summary_statistics[iter] == 'Boxplot [x,y,z]'){
    if(length(parameter_split)!=3){print(glue('Warning: entrie {iter}does not contain the correct number of parameters'));next}
    df$mean[iter]<-parameter_split[2]
    df$sd[iter]<-(parameter_split[3] - parameter_split[1])/ 1.35
  }
}


write_rds(df,path = glue('assets/table/Animal_behaviour_preprocessed.rds.gz'),compress = 'gz')
```
