Mouse meta behaviour
================

## Loading relevant libraries

Output session information to allow for replication.

``` r
#General utility packages
library(tidyverse)
library(lubridate)
library(glue)
library(knitr)

#Stats packages
library(lme4)
library(multcomp)
library(parameters)
library(effectsize)
library(performance)

#Plot pacakges
library(ggpubr)
library(wesanderson)

# Load home-built functions and global parameters
source('assets/functions/meta_behav_function.R')
source('assets/functions/perm_summary.R')
source('assets/functions/global_parameters.R')
```

``` r
#output session info
sessionInfo()
```

    ## R version 4.0.4 (2021-02-15)
    ## Platform: x86_64-pc-linux-gnu (64-bit)
    ## Running under: Pop!_OS 21.04
    ## 
    ## Matrix products: default
    ## BLAS:   /usr/lib/x86_64-linux-gnu/blas/libblas.so.3.9.0
    ## LAPACK: /usr/lib/x86_64-linux-gnu/lapack/liblapack.so.3.9.0
    ## 
    ## locale:
    ##  [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
    ##  [3] LC_TIME=en_GB.UTF-8        LC_COLLATE=en_US.UTF-8    
    ##  [5] LC_MONETARY=en_GB.UTF-8    LC_MESSAGES=en_US.UTF-8   
    ##  [7] LC_PAPER=en_GB.UTF-8       LC_NAME=C                 
    ##  [9] LC_ADDRESS=C               LC_TELEPHONE=C            
    ## [11] LC_MEASUREMENT=en_GB.UTF-8 LC_IDENTIFICATION=C       
    ## 
    ## attached base packages:
    ## [1] stats     graphics  grDevices utils     datasets  methods   base     
    ## 
    ## other attached packages:
    ##  [1] wesanderson_0.3.6  ggpubr_0.4.0       performance_0.7.2  effectsize_0.4.4-1
    ##  [5] parameters_0.13.0  multcomp_1.4-16    TH.data_1.0-10     MASS_7.3-53.1     
    ##  [9] survival_3.2-7     mvtnorm_1.1-1      lme4_1.1-27        Matrix_1.3-2      
    ## [13] knitr_1.31         glue_1.4.2         lubridate_1.7.10   forcats_0.5.1     
    ## [17] stringr_1.4.0      dplyr_1.0.6        purrr_0.3.4        readr_1.4.0       
    ## [21] tidyr_1.1.3        tibble_3.1.2       ggplot2_3.3.3      tidyverse_1.3.1   
    ## 
    ## loaded via a namespace (and not attached):
    ##  [1] nlme_3.1-152      fs_1.5.0          insight_0.14.0    httr_1.4.2       
    ##  [5] tools_4.0.4       backports_1.2.1   utf8_1.2.1        R6_2.5.0         
    ##  [9] DBI_1.1.1         colorspace_2.0-0  withr_2.4.1       tidyselect_1.1.1 
    ## [13] curl_4.3.1        compiler_4.0.4    cli_2.5.0         rvest_1.0.0      
    ## [17] xml2_1.3.2        sandwich_3.0-0    bayestestR_0.9.0  scales_1.1.1     
    ## [21] digest_0.6.27     foreign_0.8-81    minqa_1.2.4       rmarkdown_2.6    
    ## [25] rio_0.5.26        pkgconfig_2.0.3   htmltools_0.5.1.1 dbplyr_2.1.1     
    ## [29] rlang_0.4.11      readxl_1.3.1      rstudioapi_0.13   generics_0.1.0   
    ## [33] zoo_1.8-8         jsonlite_1.7.2    zip_2.1.1         car_3.0-10       
    ## [37] magrittr_2.0.1    Rcpp_1.0.6        munsell_0.5.0     fansi_0.5.0      
    ## [41] abind_1.4-5       lifecycle_1.0.0   stringi_1.6.2     yaml_2.2.1       
    ## [45] carData_3.0-4     grid_4.0.4        crayon_1.4.1      lattice_0.20-41  
    ## [49] haven_2.4.1       splines_4.0.4     hms_1.0.0         pillar_1.6.1     
    ## [53] boot_1.3-27       ggsignif_0.6.1    codetools_0.2-18  reprex_2.0.0     
    ## [57] evaluate_0.14     data.table_1.13.6 modelr_0.1.8      vctrs_0.3.8      
    ## [61] nloptr_1.2.2.2    cellranger_1.1.0  gtable_0.3.0      assertthat_0.2.1 
    ## [65] xfun_0.20         openxlsx_4.2.3    broom_0.7.6       rstatix_0.7.0    
    ## [69] ellipsis_0.3.2

![packages](assets/stickr/logo.png) Package stickers for the packaged
used in this project.

## Project outline

The analysis plan is defined in a
[pre-registration](https://osf.io/8avnb). The outline is as follows: 1.
[Load the database](#load-and-examine-table)  
2. [Standardize descriptive statistics to mean and standard
deviation](#convert-summary-statistics-to-mean-and-sd)  
3. [Provide a summary of the database](#plot-database-summary)  
4. [Generate synthetic data based on the descriptive
statistics](#generate-synthetic-data)  
5. [Estimate descriptive statistics per
groups](#export-table-descriptive-summary-per-group)  
6. Estimate effect size using a MAIN model (synthetic \~ age + sex +
strain)  
7. Estimate effect size when covariate are added, MAIN+covariate model
(synthetic \~ age + sex + strain + covariates)  
8. Estimate effect size of the remaining covariates (SECONDARY model).

The output is as follows: 1. Figure 1: database description  
2. Figure 2: descriptive statistics per group and MAIN model for OF:
time spend in center  
3. Figure 3: effect size of MAIN vs MAIN+covariate, and SECONDARY model
for OF: time spend in center  
4. Figure 4: descriptive stats per group for other experimental
parameters  
5. Supplemental Figure 1: Model plausibility  
6. Supplemental Figure 2-4: descriptive statistics per group and MAIN
model for other experimental parameters  
7. Table 1: descriptive statistics per group across experimental
parameters  
8. Table 2: effect size of MAIN across experimental parameters  
9: Table 3: Comparisons with prior studies.

## Load and examine table

Here we load the table, reformat the columns, and reorder the factors
for consistancy. Output table summary for QA/QC.

``` r
# load the table
df <-
  read_tsv('assets/table/Animal_behaviour_summary_stats.tsv',
           col_types = cols()) %>%
  
  #rename column names
  rename(Experiment.parameter = `Experiment+parameter`) %>%
  rename(Humidity = `Humidity%`) %>%
  rename(Arena_dimensions = `Arena_dimensions(mxm(+height)_or_diameter)`) %>%
  rename(Light.dark_cycle = `Light/dark_cycle`) %>%
  rename(Habituation_period =
           `Habituation_period(min_in_arena_before_start_of_test)`) %>%
  rename(Test_frequency =
           `Test_frequency(amount_of_times_test_is_repeated_to_get_to_summary_statistics)`) %>%
  rename(Saline.injection = `Saline injection`) %>%
  
  #rearrange Age_in_months
  mutate(Age_in_months = factor(Age_in_months)) %>%
  mutate(Age_in_months = fct_expand(Age_in_months, '>10')) %>%
  mutate(Age_in_months = replace(
    Age_in_months,
    Age_in_months %in% c('10-12', '12-14', '14-16', '16-18', '18-20', '20-22', '22-24'),
    '>10'
  )) %>%
  mutate(Age_in_months = fct_drop(Age_in_months)) %>%
  mutate(
    Age_in_months = fct_relevel(
      Age_in_months,
      '0-2',
      '2-4',
      '4-6',
      '6-8',
      '8-10',
      '>10',
      'Mixed age',
      'Unknown'
    )
  ) %>%
  mutate(Age_bin = factor(ifelse(
    Age_in_months %in% c('0-2', '2-4'), 'young', 'old'
  ))) %>%
  mutate(Age_bin = replace(Age_bin, Age_in_months %in% c('Mixed age', 'Unknown'), NA)) %>%
  
  #rearrange other factorial columns
  mutate(Sex = factor(Sex, levels = c("Female", "Male", "Mixed sex"))) %>%
  mutate(PMID = factor(PMID)) %>%
  mutate(Mouse_strain = factor(
    Mouse_strain,
    levels = c(
      "C57BL/6J",
      "BALB/cJ",
      "C57BL/6N",
      "DBA/2J",
      "129S1/SvImJ",
      "129X1/SvJ",
      "A/J",
      "AKR/J",
      "BALB/cByJ",
      "C3H/He",
      "C3H/HeJ",
      "C57BL/10J",
      "C57BL/6NTac",
      "C57BR",
      "CBA/J",
      "DBA/1J",
      "DBA/N",
      "FVB/N",
      "SJL",
      "Other"
    )
  )) %>%
  
  mutate(Experiment.parameter = factor(Experiment.parameter)) %>%
  mutate(Entry = factor(Entry)) %>%
  mutate(Summary_statistics = factor(Summary_statistics)) %>%
  mutate(Time_of_day = factor(Time_of_day)) %>%
  mutate(Light.dark_cycle = factor(Light.dark_cycle)) %>%
  mutate(Housing_conditions = factor(Housing_conditions)) %>%
  mutate(Saline.injection = factor(Saline.injection)) %>%
  mutate(Timestamp = mdy_hms(Timestamp, tz = 'CET')) %>%
  mutate(Luminosity_lux = as.numeric(Luminosity_lux))


# change mouse strain to other everything that isn't C57BL/6J, DBA/2J, BALB/cJ, or C57BL/6N
# make a dplyr/forcat conversion if possible
levels(df$Mouse_strain) <- c(levels(df$Mouse_strain), "Other")
df$Mouse_strain[!df$Mouse_strain %in% c('C57BL/6J', 'DBA/2J', 'C57BL/6N','BALB/cJ')]<-'Other'

# change age to >10 for every age group 10 months or older. 
# make a dplyr/forcat conversion if possible
levels(df$Age_in_months) <- c(levels(df$Age_in_months), ">10")
df$Age_in_months[df$Age_in_months %in% c("10-12","12-14","14-16","16-18","18-20","20-22","22-24")]<-'>10'


# table summary
summary(df) %>% kable("pipe")
```

|     | Timestamp                   | PMID          | Sample\_size  | Sex           | Age\_in\_months | Mouse\_strain  | Experiment.parameter                   | Test\_duration\_total\_min | Luminosity\_lux | Water\_temperature\_celsius | Humidity      | Color\_of\_arena | Shape\_of\_arena | Entry      | Summary\_statistics    | Parameter\_data  | Arena\_dimensions | Noise\_level\_dB | Time\_of\_day             | Light.dark\_cycle   | Housing\_conditions | Habituation\_period | Test\_frequency | Handling\_duration\_min(period\_immediately\_prior\_to\_testing) | Saline.injection | Age\_bin  |
|:----|:----------------------------|:--------------|:--------------|:--------------|:----------------|:---------------|:---------------------------------------|:---------------------------|:----------------|:----------------------------|:--------------|:-----------------|:-----------------|:-----------|:-----------------------|:-----------------|:------------------|:-----------------|:--------------------------|:--------------------|:--------------------|:--------------------|:----------------|:-----------------------------------------------------------------|:-----------------|:----------|
|     | Min. :2021-03-03 14:25:08   | 23288504: 59  | Min. : 4.00   | Female :237   | 2-4 :844        | C57BL/6J :867  | EPM: Distance travelled (m/min) : 54   | Min. : 1.000               | Min. : 0.0      | Min. : 0.00                 | Min. :40.00   | Length:1370      | Length:1370      | Hannah:703 | Boxplot \[x,y,z\]: 10  | Length:1370      | Length:1370       | Min. :40.00      | Afternoon (12:00-0:00: 53 | Inverted (D-L): 137 | Group :708          | Min. : 0.50         | Min. :2.000     | Min. : 0.70                                                      | Yes : 206        | old :232  |
|     | 1st Qu.:2021-03-11 14:49:31 | 22028789: 37  | 1st Qu.: 9.00 | Male :993     | 0-2 :154        | BALB/cJ :185   | EPM: Time spent in closed arms (%):215 | 1st Qu.: 5.000             | 1st Qu.: 40.0   | 1st Qu.:24.00               | 1st Qu.:50.00 | Class :character | Class :character | Sareen:667 | Mean+SD \[x,y\] : 81   | Class :character | Class :character  | 1st Qu.:55.00    | Morning (0:00-12:00) : 97 | Normal (L-D) :1081  | Single:253          | 1st Qu.: 2.00       | 1st Qu.:2.000   | 1st Qu.: 30.00                                                   | NA’s:1164        | young:998 |
|     | Median :2021-03-18 22:42:04 | 19931548: 29  | Median :11.00 | Mixed sex:140 | 4-6 :108        | Other :168     | EPM: Time spent in open arms (%) :249  | Median : 5.000             | Median : 100.0  | Median :25.00               | Median :50.00 | Mode :character  | Mode :character  |            | Mean+SEM \[x,y\] :1279 | Mode :character  | Mode :character   | Median :55.00    | NA’s :1220                | NA’s : 152          | NA’s :409           | Median : 2.00       | Median :3.000   | Median : 60.00                                                   |                  | NA’s :140 |
|     | Mean :2021-03-28 18:02:30   | 28104556: 24  | Mean :12.56   |               | Unknown : 87    | C57BL/6N : 82  | FST: Immobility time (%) :178          | Mean : 8.975               | Mean : 171.8    | Mean :24.46                 | Mean :52.56   |                  |                  |            |                        |                  |                   | Mean :58.94      |                           |                     |                     | Mean : 5.09         | Mean :2.622     | Mean : 47.64                                                     |                  |           |
|     | 3rd Qu.:2021-04-14 22:56:24 | 17098297: 21  | 3rd Qu.:14.00 |               | 6-8 : 59        | DBA/2J : 68    | FST: Time spent swimming (%) : 19      | 3rd Qu.:10.000             | 3rd Qu.: 200.0  | 3rd Qu.:25.00               | 3rd Qu.:58.75 |                  |                  |            |                        |                  |                   | 3rd Qu.:63.75    |                           |                     |                     | 3rd Qu.:10.00       | 3rd Qu.:3.000   | 3rd Qu.: 60.00                                                   |                  |           |
|     | Max. :2021-05-20 18:20:20   | 18453006: 21  | Max. :50.00   |               | Mixed age: 53   | 129S1/SvImJ: 0 | OF: Distance travelled (m/min) :342    | Max. :60.000               | Max. :1600.0    | Max. :35.00                 | Max. :70.00   |                  |                  |            |                        |                  |                   | Max. :77.00      |                           |                     |                     | Max. :15.00         | Max. :3.000     | Max. :120.00                                                     |                  |           |
|     |                             | (Other) :1179 |               |               | (Other) : 65    | (Other) : 0    | OF: Time spent in center (%) :313      | NA’s :4                    | NA’s :882       | NA’s :1194                  | NA’s :1152    |                  |                  |            |                        |                  |                   | NA’s :1320       |                           |                     |                     | NA’s :1312          | NA’s :1333      | NA’s :1267                                                       |                  |           |

## Convert summary statistics to mean and SD

At the moment, the database only contains Mean+SEM, Mean+SD, or boxplot
data. See [pre-registration](https://osf.io/8avnb) for other
transformations if necessary.

``` r
df$mean <- c()
df$sd <- c()

# for every row in df
for(iter in 1:dim(df)[1]){
  #split the parameters, there should be two entries for mean+SEM and Mean+Sd and three entries from boxplot. 
  #Ive added a check for parameter length with warning message if this isn't the case. 
parameter_split <- as.numeric(unlist(strsplit(as.character(df$Parameter_data[iter]),', ')))
  #case Mean+SEM
  if (df$Summary_statistics[iter] == 'Mean+SEM [x,y]'){
    if(length(parameter_split)!=2){print(glue('Warning: entrie {iter}does not contain the correct number of parameters'));next}
    df$mean[iter]<-parameter_split[1]
    df$sd[iter]<-parameter_split[2] * sqrt(df$Sample_size[iter])
    
  #case Mean+SD
  }else if (df$Summary_statistics[iter] == 'Mean+SD [x,y]'){
    if(length(parameter_split)!=2){print(glue('Warning: entrie {iter}does not contain the correct number of parameters'));next}
    df$mean[iter]<-parameter_split[1]
    df$sd[iter]<-parameter_split[2]
    
  #case Boxplot
  }else if (df$Summary_statistics[iter] == 'Boxplot [x,y,z]'){
    if(length(parameter_split)!=3){print(glue('Warning: entrie {iter}does not contain the correct number of parameters'));next}
    df$mean[iter]<-parameter_split[2]
    df$sd[iter]<-(parameter_split[3] - parameter_split[1])/ 1.35
  }
}
```

## Plot database summary

Figure 1.  
A) Experiment type (pie plot) B) Strain (pie plot) C) Sex (pie plot) D)
Age (bar plot) E) Sample size (histogram)

``` r
exp <- df %>%  group_by(Experiment.parameter) %>% 
                        summarise(Count = n()) %>% mutate(ypos = sum(Count)-cumsum(Count) + (Count/2)) 
exp$Exp.short<-c('','EPM Closed','EPM Open','FST Imm','','OF Dist','OF Time')


A <- ggplot(exp, aes(x="", y=Count, fill=Experiment.parameter)) +
    geom_bar(stat="identity", width=1, color="white") +
    coord_polar("y", start=0) +
    geom_text(x=xpos, aes(y=ypos, label=Exp.short), check_overlap = FALSE, size=2)+
    scale_fill_manual(values=pal)+
    theme_void()+
    theme(text = element_text(size = 6))+
    theme(legend.position="none")


strain <- df %>%  group_by(Mouse_strain) %>% 
                        summarise(Count = n()) %>% mutate(ypos = sum(Count)-cumsum(Count) + (Count/2)) 


B <- ggplot(strain, aes(x="", y=Count, fill=Mouse_strain)) +
    geom_bar(stat="identity", width=1, color="white") +
    coord_polar("y", start=0) +
    geom_text(x=xpos, aes(y=ypos, label=Mouse_strain), check_overlap = FALSE, size=2)+
    scale_fill_manual(values=pal)+
    theme_void()+
    theme(text = element_text(size = 6))+
    theme(legend.position="none")

sex <- df %>% 
          group_by(Sex)%>% 
                        summarise(Count = n()) %>% mutate(ypos = sum(Count)-cumsum(Count) + (Count/2)) 


C <- ggplot(sex, aes(x='', y=Count, fill=Sex)) +
    geom_bar(stat="identity", width=1, color="white") +
    coord_polar("y", start=0) +
      geom_text(x=xpos, aes(y=ypos, label=Sex), check_overlap = FALSE, size=2)+
    scale_fill_manual(values=pal)+
    theme_void()+
    theme(text = element_text(size = 6))+
    theme(legend.position="none")


age <- df %>% group_by(Age_in_months)%>% 
          summarise(Count = n()) 

D <- ggplot(age, aes(x=Age_in_months, y=Count, group=Age_in_months)) +
    geom_bar(stat="identity", width=1, color="white",position = position_dodge(width = 1), fill=pal[1]) +
    theme_light()+
    theme(text = element_text(size = 6), axis.text.x = element_text(angle = 45, vjust = 0.5))+
    labs(x='Age [Months]',y ='Count')

E <- ggplot(df, aes(x=Sample_size)) + 
      geom_histogram(fill=pal[1],binwidth=2)+
    theme_light()+
    theme(text = element_text(size = 6))+
    labs(x='Sample size',y ='Count')  


fig1 <- ggarrange(ggarrange(A, B, C, ncol = 3, labels = c("A","B", "C"),font.label = list(size = 8, color = "black", face = "bold", family = NULL)),
          ggarrange(D, E, ncol = 2, labels = c("D","E"),font.label = list(size = 8, color = "black", face = "bold", family = NULL)),
          nrow = 2
          ) 

ggsave('assets/plot/Figure1.svg',plot = fig1, device = 'svg', width=90,height = 70,units = 'mm',dpi = 300)
ggsave('assets/plot/Figure1.png',plot = fig1, device = 'png', width=90,height = 70,units = 'mm',dpi = 300)

#output experiment type, strain, and sex to add as labels in pie plots later

exp %>% dplyr::select(Experiment.parameter,Count) %>%  kable("pipe")

strain %>% dplyr::select(Mouse_strain,Count) %>%  kable("pipe")

sex %>% dplyr::select(Sex,Count) %>%  kable("pipe")
```

| Experiment.parameter               | Count |
|:-----------------------------------|------:|
| EPM: Distance travelled (m/min)    |    54 |
| EPM: Time spent in closed arms (%) |   215 |
| EPM: Time spent in open arms (%)   |   249 |
| FST: Immobility time (%)           |   178 |
| FST: Time spent swimming (%)       |    19 |
| OF: Distance travelled (m/min)     |   342 |
| OF: Time spent in center (%)       |   313 |

| Mouse\_strain | Count |
|:--------------|------:|
| C57BL/6J      |   867 |
| BALB/cJ       |   185 |
| C57BL/6N      |    82 |
| DBA/2J        |    68 |
| Other         |   168 |

| Sex       | Count |
|:----------|------:|
| Female    |   237 |
| Male      |   993 |
| Mixed sex |   140 |

Tables above describing the number of database entries per conditions.

![Figure 1](assets/plot/Figure1.png) **Figure 1. Database description.**
Parameter distribution for Experiment parameter (A), Strain (B), Sex
(C), Age (D), Sample size (E).

## Generate synthetic data

In the section below, we generate synthetic data. First, we split the
table as a function of experimental parameter. We also need to select
covariate for the MAIN+CoVariate and SECONDAY analysis. Unfortunately,
the list of covariate is restricted by data availability. Covariates are
selected to retain \~50% of the data entries.

The summary statistics and effect sizes are directly outputed into
tables in `assets/table` at the end of the `perm_summary` function for
later retrieval. Because this chunk is computationally heavy, it isn’t
run by default in the code.

Note, this will take a long long time to run with 10’000 permutations.
:-P

``` r
#filter out main data.frame 
df <- df %>% filter(df$Sex != 'Mixed sex', df$Mouse_strain != 'Other', !df$Age_in_months %in% c('Mixed age','Unknown'))

#full list of potential covariates
covariate.list<- c("Test_duration_total_min", "Luminosity_lux","Water_temperature_celsius"   ,"Humidity"  ,"Color_of_arena","Shape_of_arena" , "Arena_dimensions","Noise_level_dB","Time_of_day", "Light.dark_cycle","Housing_conditions", "Habituation_period" , "Test_frequency","Handling_duration_min(period_immediately_prior_to_testing)","Saline.injection")


##OF: Time spent in center (%)
exp.type <- 'OF: Time spent in center (%)'
exp.type.short<- 'OF_time_center'

df.sub <- df %>% filter(Experiment.parameter == exp.type) %>% 
                    mutate_if(is.factor,fct_drop)
#levels(df.sub$Sex)
#levels(df.sub$Mouse_strain)
#levels(df.sub$Age_bin)
# 
#summary(df.sub[,names(df.sub) %in% covariate.list])
covariates<-c('Test_duration_total_min','Luminosity_lux', 'Light.dark_cycle')

df.sub[,names(df.sub)%in% covariates] %>%   complete.cases() %>%   sum() %>% {glue('Remaining number of entries {.} / {nrow(df.sub)}')}
#[1] "Remaining number of entries 115 / 225"

perm_summary(exp.type.short, df.sub,permutation, covariates)


##OF: Distance travelled (m/min)
exp.type <- 'OF: Distance travelled (m/min)'
exp.type.short<- 'OF_distance'

df.sub <- df %>% filter(Experiment.parameter == exp.type) %>% 
                    mutate_if(is.factor,fct_drop)
#levels(df.sub$Sex)
#levels(df.sub$Mouse_strain)
#levels(df.sub$Age_bin)
# 
#summary(df.sub[,names(df.sub) %in% covariate.list])
covariates<-c('Test_duration_total_min','Luminosity_lux', 'Light.dark_cycle')

df.sub[,names(df.sub)%in% covariates] %>%   complete.cases() %>%   sum() %>% {glue('Remaining number of entries {.} / {nrow(df.sub)}')}
#[1] "Remaining number of entries 106 / 246"

perm_summary(exp.type.short, df.sub,permutation, covariates)


##EPM: Time spent in closed arms (%)
exp.type <- "EPM: Time spent in closed arms (%)"
exp.type.short<- 'EPM_time_closed'

df.sub <- df %>% filter(Experiment.parameter == exp.type) %>% 
                    mutate_if(is.factor,fct_drop)
# levels(df.sub$Sex)
# levels(df.sub$Mouse_strain)
# levels(df.sub$Age_bin)
# 
# summary(df.sub[,names(df.sub) %in% covariate.list])
covariates<-c('Test_duration_total_min','Luminosity_lux', 'Light.dark_cycle')

df.sub[,names(df.sub)%in% covariates] %>%   complete.cases() %>%   sum() %>% {glue('Remaining number of entries {.} / {nrow(df.sub)}')}
#[1] "Remaining number of entries 58 / 150"

perm_summary(exp.type.short, df.sub,permutation, covariates)


##EPM: Time spent in open arms (%)
exp.type <- "EPM: Time spent in open arms (%)"
exp.type.short<- 'EPM_time_open'

df.sub <- df %>% filter(Experiment.parameter == exp.type) %>% 
                    mutate_if(is.factor,fct_drop)
# levels(df.sub$Sex)
# levels(df.sub$Mouse_strain)
# levels(df.sub$Age_bin)
# 
# summary(df.sub[,names(df.sub) %in% covariate.list])
covariates<-c('Test_duration_total_min','Luminosity_lux', 'Light.dark_cycle')

df.sub[,names(df.sub)%in% covariates] %>%   complete.cases() %>%   sum() %>% {glue('Remaining number of entries {.} / {nrow(df.sub)}')}
#[1] "Remaining number of entries 65 / 169"

perm_summary(exp.type.short, df.sub,permutation, covariates)


##FST: Immobility time (%)
exp.type <- "FST: Immobility time (%)"
exp.type.short<- 'FST_immobility'

df.sub <- df %>% filter(Experiment.parameter == exp.type) %>% 
                    mutate_if(is.factor,fct_drop)
# levels(df.sub$Sex)
# levels(df.sub$Mouse_strain)
# levels(df.sub$Age_bin)
# 
# summary(df.sub[,names(df.sub) %in% covariate.list])
covariates<-c('Test_duration_total_min', 'Water_temperature_celsius', 'Light.dark_cycle')

df.sub[,names(df.sub)%in% covariates] %>%   complete.cases() %>%   sum() %>% {glue('Remaining number of entries {.} / {nrow(df.sub)}')}
#[1] "Remaining number of entries 87 / 114"

perm_summary(exp.type.short, df.sub,permutation, covariates)
```

## Analysis for Open Field: Time spent in the center

``` r
df <-
  df %>% filter(
    df$Sex != 'Mixed sex',
    df$Mouse_strain != 'Other',
    !df$Age_in_months %in% c('Mixed age', 'Unknown')
  )

exp.type <- 'OF: Time spent in center (%)'
exp.type.label <- "Open Field: \n Time spent in center (%)"
exp.type.short <- 'OF_time_center'

df.sub <- df %>% filter(Experiment.parameter == exp.type) %>%
  mutate_if(is.factor, fct_drop)

sum.stats <-
  read_tsv(glue('assets/table/summary_stat_{exp.type.short}.tsv.gz'),
           col_types = cols()) %>%
  mutate(Sex = factor(Sex, levels = c("Female", "Male"))) %>%
  mutate(Mouse_strain = factor(
    Mouse_strain,
    levels = c("C57BL/6J",
               "BALB/cJ",
               "C57BL/6N",
               "DBA/2J")
  )) %>%
  mutate(Age_in_months = factor(
    Age_in_months,
    levels = c('0-2',
               '2-4',
               '4-6',
               '6-8',
               '8-10',
               '>10')
  ))

sum.stats.noage <-
  read_tsv(glue('assets/table/summary_stat_noage_{exp.type.short}.tsv.gz'),
           col_types = cols()) %>%
  mutate(Sex = factor(Sex, levels = c("Female", "Male"))) %>%
  mutate(Mouse_strain = factor(
    Mouse_strain,
    levels = c("C57BL/6J",
               "BALB/cJ",
               "C57BL/6N",
               "DBA/2J")
  )) 
  
  effect.size <-
  read_tsv(glue('assets/table/effect_size_{exp.type.short}.tsv.gz'),
           col_types = cols())
effect.size.cov <-
  read_tsv(glue('assets/table/effect_size_cov_{exp.type.short}.tsv.gz'),
           col_types = cols())


fig2 <-
  plot_summary(exp.type,
               df.sub,
               sum.stats,
               sum.stats.noage,
               effect.size,
               exp.type.label)
ggsave(
  'assets/plot/Figure2.svg',
  plot = fig2,
  device = 'svg',
  width = 180,
  height = 85,
  units = 'mm',
  dpi = 300
)
ggsave(
  'assets/plot/Figure2.png',
  plot = fig2,
  device = 'png',
  width = 180,
  height = 85,
  units = 'mm',
  dpi = 300
)
```

![Figure 2](assets/plot/Figure2.png) **Figure 2. Open field meta
analysis.** A) Number of database entries per condition. B) Estimated
time spent in the center of the open field parameter as a function of
strain and sex (Sex colour label to be added). Bars plus black error
bars indicate the bootstrapped group mean (50th quantile ± \[5th,
95th\]), coloured error bars indicate ± 1 standard deviation. C)
Estimated parameter in C57BL/6j as a function of time and sex (Sex
colour label to be added). Solid and dashed lines indicate the
bootstrapped group mean (50th quantile ± \[5th, 95th\]). Sex (D), age
(E), and strain (F) effects size distribution indicated as Cohen’s
d. Solid and dashed lines indicate the 5th, 50th, and 95th quantiles.

``` r
covariates<-c('Test_duration_total_min','Luminosity_lux', 'Light.dark_cycle')
df.sub.covariates <- df.sub[,names(df.sub)%in% covariates] %>% complete.cases() %>% df.sub[.,]  %>% mutate_if(is.factor,fct_drop)
  
fig_tmp<-df.sub.covariates %>% 
          group_by(Sex, Age_in_months,Mouse_strain) %>% 
          summarise(Count=length(Sample_size),.groups = 'drop') %>% 
          ggballoonplot(x = "Mouse_strain", y = "Age_in_months",
          size = "Count", fill = "Count",
          facet.by = c("Sex"),
          font.main = c(6,"bold.italic", "black"),
          font.x = c(6, "plain", "black"),
          font.y = c(6, "plain", "black"),
          font.legend= c(6, "plain", "black"),
          font.submain= c(6, "plain", "black"),
          font.caption= c(6, "plain", "black"),
          font.tickslab= c(6, "plain", "black"),
          font.xtickslab= c(6, "plain", "black"),
          font.ytickslab= c(6, "plain", "black"),
          xlab = '',
          ylab = '',
          ggtheme = theme_bw())+ 
          scale_fill_gradientn(colors = pal) 

ggsave('assets/plot/misc/Figure_OF_time_covariate_summary.svg', plot = fig_tmp, device = 'svg', width=90,height = 85,units = 'mm',dpi = 300) 
ggsave('assets/plot/misc/Figure_OF_time_covariate_summary.png',  plot = fig_tmp, device = 'png', width=90,height = 85,units = 'mm',dpi = 300)


  effect.size.sum <- effect.size.cov %>% 
                          gather('effect', 'effect.size') %>% 
                          separate(effect, c('effect','covariate'), fill='right') %>%  
                          replace(is.na(.),'simple') %>% 
                           mutate(effect = as_factor(effect), covariate = as_factor(covariate)) %>% 
                          mutate(covariate = fct_relevel(covariate, "simple", "cov")) 

  A <- effect.size.sum %>% 
        filter(effect %in% c('Age','Sex','Strain')) %>% 
        ggplot(aes(x=effect, y=effect.size, fill=covariate))+
        geom_violin()+
        geom_hline(aes(yintercept = 0.2) ,linetype='longdash', color=pal[3])+ 
        theme_light()+
        theme(text = element_text(size = 6),
           axis.text.x = element_text(angle = 45, vjust = 0.5),
          legend.position = c(0.25,0.85),
          legend.key.size = unit(6, "pt"),
          legend.background = element_rect(fill = alpha('white', 0)),
          legend.title = element_blank())+
        scale_fill_manual(values=pal[c(3,6)])+
        scale_colour_manual(values=pal[c(3,6)])+
        ylim(0,max(effect.size.sum$effect.size)*1.1)+
        labs(x='Effect', y='Effect size [Cohen\'f]')
      
     
B <- effect.size.sum %>% 
        filter(effect %in% c('Test', 'Luminosity', 'Light')) %>% 
        ggplot(aes(x=effect, y=effect.size))+
        geom_violin(fill=pal[6])+
        geom_hline(aes(yintercept = 0.2) ,linetype='longdash', color=pal[3])+ 
        theme_light()+
        theme(text = element_text(size = 6),
           axis.text.x = element_text(angle = 45, vjust = 0.5),
          legend.position = "none")+
        ylim(0,max(effect.size.sum$effect.size)*1.1)+
        labs(x='Effect', y='Effect size [Cohen\'f]')


C <- df.sub.covariates %>% 
      ggplot(aes(x = Luminosity_lux, y = mean,  fill = Sex, colour = Sex))+
      geom_smooth(method='lm', formula= y~x)+
      geom_point(size=0.2)+
      theme_light()+
      theme(text = element_text(size = 6),
          legend.position = c(0.85,0.85),
          legend.key.size = unit(6, "pt"),
          legend.background = element_rect(fill = alpha('white', 0)),
          legend.title = element_blank())+
      scale_fill_manual(values=pal[c(2,5)])+
      scale_colour_manual(values=pal[c(2,5)])+ 
      labs(x='Luminosity [Lux]', y=exp.type)

D <- df.sub.covariates %>% 
      ggplot(aes(x = Test_duration_total_min, y = mean,  fill = Sex, colour = Sex))+
      geom_smooth(method='lm', formula= y~x)+
      geom_point(size=0.2)+
      theme_light()+
      theme(text = element_text(size = 6),
          legend.position = c(0.85,0.85),
          legend.key.size = unit(6, "pt"),
          legend.background = element_rect(fill = alpha('white', 0)),
          legend.title = element_blank())+
      scale_fill_manual(values=pal[c(2,5)])+
      scale_colour_manual(values=pal[c(2,5)])+ 
      labs(x='Test duration [min]', y=exp.type)


E <- df.sub.covariates %>% 
      ggplot(aes(x = Light.dark_cycle, y = mean,  fill = Light.dark_cycle, colour = Light.dark_cycle))+
      geom_violin()+
      geom_boxplot(width=0.1, fill='white', colour='lightgrey',outlier.shape = NA)+
      theme_light()+
      theme(text = element_text(size = 6),
           axis.text.x = element_text(angle = 45, vjust = 0.5),
          legend.position = "none")+
      scale_fill_manual(values=pal[c(2,5)])+
      scale_colour_manual(values=pal[c(2,5)])+ 
      labs(x='Test duration [min]', y=exp.type)

fig3 <- ggarrange(ggarrange(A,
                            B,
                            ncol = 2,
                            labels=c('A','B'),
                            font.label = list(size = 8, color = "black", face = "bold", family = NULL)), 
                  C, 
                  D, 
                  nrow = 3, 
                  labels=c('','C', 'D'),
                  font.label = list(size = 8, color = "black", face = "bold", family = NULL))

ggsave('assets/plot/Figure3.svg',plot = fig3, device = 'svg', width=90,height = 100,units = 'mm',dpi = 300)
ggsave('assets/plot/Figure3.png',plot = fig3, device = 'png', width=90,height = 100,units = 'mm',dpi = 300)
```

![Figure 3](assets/plot/Figure3.png) **Figure 3. Covariate analysis.**
A) Comparison of fixed normalized effect in a simple model (synthetic \~
Age + Sex + Strain, green) vs model plus covariates (simple mode + Test
duration + Luminosity + Shape of the open field). The addition of
covariates to the model do not affect the effect size for Age, Sex, or
Strain. Dashed line indicates the threshold between very small and small
effect size according to Cohen 1988 guidelines. (Colour code to be
added) B) Effect size for Test duration, Luminosity, and Shape of the
open field. Time spent in the center of the open field as a function of
Sex and Luminosity (C) or test duration (Sex colour to be added) (D).
Points indicate single database entries. Line and ribbon indicate linear
model parameters with 5th and 95th confidence intervals.

``` r
df.new<-synth_gen(df.sub)

mod.simple <- lm(Synthetic~Age_bin+Sex+Mouse_strain, data=df.new)

check_model(mod.simple)%>% ggexport(filename = "assets/plot/Figure_S1.png",pointsize = 6, width = 2000, height = 2500,res = 300) 
```

    ## Loading required namespace: qqplotr

    ## file saved to assets/plot/Figure_S1.png

``` r
print('Tested model assumption for :')
print(formula(mod.simple))
model_performance(mod.simple)
```

    ## [1] "Tested model assumption for :"
    ## Synthetic ~ Age_bin + Sex + Mouse_strain
    ## # Indices of model performance
    ## 
    ## AIC       |       BIC |    R2 | R2 (adj.) |   RMSE |  Sigma
    ## -----------------------------------------------------------
    ## 22214.182 | 22255.909 | 0.056 |     0.054 | 11.620 | 11.632

![Figure S1](assets/plot/Figure_S1.png) **Supplemental Figure 1. Model
assumption test.** The model (Synthetic \~ Age + Sex + Strain) for the
time spent in the center of the open field appears plausible. The plots
for the normality of the residuals indicate a short tail and heavy head.

We conclude that the models we generated are plausible, albeit not
perfects due to short tail and heavy head. This is naturally to be
expected as the parameters are bounded at 0 (can’t have negative time
spent in open field.)

## Make model with covariates

``` r
df.new<-synth_gen(df.sub.covariates)

mod.covariates <- lm(Synthetic~Age_bin+
                              Sex+
                              Mouse_strain+
                              Test_duration_total_min+
                              Luminosity_lux+
                              Shape_of_arena, data=df.new)

mod.simple <- lm(Synthetic~Age_bin+
                            Sex+
                            Mouse_strain, data=df.new)

print('Comparing model performance for model + extra covariates:')
print(formula(mod.covariates))
print('and model simple:')
print(formula(mod.simple))

compare_performance(mod.covariates, mod.simple, rank = TRUE)
```

    ## [1] "Comparing model performance for model + extra covariates:"
    ## Synthetic ~ Age_bin + Sex + Mouse_strain + Test_duration_total_min + 
    ##     Luminosity_lux + Shape_of_arena
    ## [1] "and model simple:"
    ## Synthetic ~ Age_bin + Sex + Mouse_strain
    ## # Comparison of Model Performance Indices
    ## 
    ## Name           | Model |       AIC |       BIC |    R2 | R2 (adj.) |  RMSE | Sigma | Performance-Score
    ## ------------------------------------------------------------------------------------------------------
    ## mod.covariates |    lm | 10243.782 | 10301.366 | 0.187 |     0.181 | 9.640 | 9.675 |           100.00%
    ## mod.simple     |    lm | 11414.189 | 11451.574 | 0.153 |     0.151 | 9.753 | 9.772 |             0.00%

We conclude that the model with covariates does not majorly influence
model performance.

## Export table descriptive summary per group

``` r
df <-
  df %>% filter(
    df$Sex != 'Mixed sex',
    df$Mouse_strain != 'Other',
    !df$Age_in_months %in% c('Mixed age', 'Unknown')
  )

exp.type.short <- 'OF_time_center'
sum.stats.noage.OF_time_center <-
  read_tsv(glue('assets/table/summary_stat_noage_{exp.type.short}.tsv.gz'),
           col_types = cols()) %>%
  mutate(Sex = factor(Sex, levels = c("Female", "Male"))) %>%
  mutate(Mouse_strain = factor(
    Mouse_strain,
    levels = c("C57BL/6J",
               "BALB/cJ",
               "C57BL/6N",
               "DBA/2J")
  )) 


exp.type.short <- 'OF_distance'
sum.stats.noage.OF_distance <-
  read_tsv(glue('assets/table/summary_stat_noage_{exp.type.short}.tsv.gz'),
           col_types = cols()) %>%
  mutate(Sex = factor(Sex, levels = c("Female", "Male"))) %>%
  mutate(Mouse_strain = factor(
    Mouse_strain,
    levels = c("C57BL/6J",
               "BALB/cJ",
               "C57BL/6N",
               "DBA/2J")
  )) 

exp.type.short <- 'FST_immobility'
sum.stats.noage.FST_immobility <-
  read_tsv(glue('assets/table/summary_stat_noage_{exp.type.short}.tsv.gz'),
           col_types = cols()) %>%
  mutate(Sex = factor(Sex, levels = c("Female", "Male"))) %>%
  mutate(Mouse_strain = factor(
    Mouse_strain,
    levels = c("C57BL/6J",
               "BALB/cJ",
               "C57BL/6N",
               "DBA/2J")
  )) 

exp.type.short <- 'EPM_time_open'
sum.stats.noage.EPM_time_open <-
  read_tsv(glue('assets/table/summary_stat_noage_{exp.type.short}.tsv.gz'),
           col_types = cols()) %>%
  mutate(Sex = factor(Sex, levels = c("Female", "Male"))) %>%
  mutate(Mouse_strain = factor(
    Mouse_strain,
    levels = c("C57BL/6J",
               "BALB/cJ",
               "C57BL/6N",
               "DBA/2J")
  )) 

exp.type.short <- 'EPM_time_closed'
sum.stats.noage.EPM_time_closed <-
  read_tsv(glue('assets/table/summary_stat_noage_{exp.type.short}.tsv.gz'),
           col_types = cols()) %>%
  mutate(Sex = factor(Sex, levels = c("Female", "Male"))) %>%
  mutate(Mouse_strain = factor(
    Mouse_strain,
    levels = c("C57BL/6J",
               "BALB/cJ",
               "C57BL/6N",
               "DBA/2J")
  )) 

sum.stats.noage.OF_time_center.quantile<- get_quantile(sum.stats.noage.OF_time_center,c('Sex','Mouse_strain'))

sum.stats.noage.OF_distance.quantile<- get_quantile(sum.stats.noage.OF_distance,c('Sex','Mouse_strain'))
A<- ggplot(sum.stats.noage.OF_distance.quantile,aes(x=Mouse_strain, y=q50, group=Sex, fill=Sex, colour=Sex))+
    geom_col(position = position_dodge2(width=0.9))+
    geom_errorbar(aes(ymin = l50, ymax = u50),position = position_dodge(width=0.9),width=0)+
    geom_errorbar(aes(ymin = q5, ymax = q95),position = position_dodge(width=0.9),width=0, colour='black')+
    theme_light()+
    theme(text = element_text(size = 6), 
          axis.text.x = element_text(angle = 45, vjust = 0.5), 
          axis.title.x = element_blank(),
          legend.position = c(0.2, 0.85), 
          legend.key.size = unit(6, "pt"),
          legend.background = element_rect(fill = alpha('white', 0)),
          legend.title = element_blank())+
    labs(y='Distance travelled (m/min)', title='OF: Distance travelled')+
    scale_fill_manual(values=pal[c(2,5)])+
    scale_colour_manual(values=pal[c(2,5)])+ 
    aes(ymin=0)

sum.stats.noage.FST_immobility.quantile<- get_quantile(sum.stats.noage.FST_immobility,c('Sex','Mouse_strain'))
B<- ggplot(sum.stats.noage.FST_immobility.quantile,aes(x=Mouse_strain, y=q50, group=Sex, fill=Sex, colour=Sex))+
    geom_col(position = position_dodge2(width=0.9))+
    geom_errorbar(aes(ymin = l50, ymax = u50),position = position_dodge(width=0.9),width=0)+
    geom_errorbar(aes(ymin = q5, ymax = q95),position = position_dodge(width=0.9),width=0, colour='black')+
    theme_light()+
    theme(text = element_text(size = 6), 
          axis.text.x = element_text(angle = 45, vjust = 0.5), 
          axis.title.x = element_blank(),
          legend.position = "none")+
    labs(y='Time spent swimming (%)', title='FST: Time spent swimming')+
    scale_fill_manual(values=pal[c(2,5)])+
    scale_colour_manual(values=pal[c(2,5)])+ 
    aes(ymin=0)


sum.stats.noage.EPM_time_open.quantile<- get_quantile(sum.stats.noage.EPM_time_open,c('Sex','Mouse_strain'))
C<- ggplot(sum.stats.noage.EPM_time_open.quantile,aes(x=Mouse_strain, y=q50, group=Sex, fill=Sex, colour=Sex))+
    geom_col(position = position_dodge2(width=0.9))+
    geom_errorbar(aes(ymin = l50, ymax = u50),position = position_dodge(width=0.9),width=0)+
    geom_errorbar(aes(ymin = q5, ymax = q95),position = position_dodge(width=0.9),width=0, colour='black')+
    theme_light()+
    theme(text = element_text(size = 6), 
          axis.text.x = element_text(angle = 45, vjust = 0.5), 
          axis.title.x = element_blank(),
          legend.position = "none")+
    labs(y='Time spent in open arms (%)', title='EPM: Time spent in open arms')+
    scale_fill_manual(values=pal[c(2,5)])+
    scale_colour_manual(values=pal[c(2,5)])+ 
    aes(ymin=0)

# EPM closed time left out due to bug for now. 

 #sum.stats.noage.EPM_time_closed.quantile<- get_quantile(sum.stats.noage.EPM_time_closed,c('Sex','Mouse_strain'))

 # D<- ggplot(sum.stats.noage.EPM_time_closed.quantile,aes(x=Mouse_strain, y=q50, group=Sex, fill=Sex, colour=Sex))+
#     geom_col(position = position_dodge2(width=0.9))+
#     geom_errorbar(aes(ymin = l50, ymax = u50),position = position_dodge(width=0.9),width=0)+
#     geom_errorbar(aes(ymin = q5, ymax = q95),position = position_dodge(width=0.9),width=0, colour='black')+
#     theme_light()+
#     theme(text = element_text(size = 6), 
#           axis.text.x = element_text(angle = 45, vjust = 0.5), 
#           axis.title.x = element_blank(),
#           legend.position = c(0.2, 0.85), 
#           legend.key.size = unit(6, "pt"),
#           legend.background = element_rect(fill = alpha('white', 0)),
#           legend.title = element_blank())+
#     labs(y=exp.type.label, title='EPM: Time spent in closed arms')+
#     scale_fill_manual(values=pal[c(2,5)])+
#     scale_colour_manual(values=pal[c(2,5)])+ 
#     aes(ymin=0)


fig4 <- ggarrange(A,
                  B,
                  C,
                  ncol = 2,
                  nrow = 2,
                  labels=c('A','B', 'C'),
                  font.label = list(size = 8, color = "black", face = "bold", family = NULL))

ggsave('assets/plot/Figure4.svg',plot = fig4, device = 'svg', width=90,height = 100,units = 'mm',dpi = 300)
ggsave('assets/plot/Figure4.png',plot = fig4, device = 'png', width=90,height = 100,units = 'mm',dpi = 300)
  


OF_time_center<-sum.stats.noage.OF_time_center.quantile %>% unite(col='group', Mouse_strain,Sex) %>% group_by(group) %>% summarise(summary = glue('{round(q50,2)} [{round(q5,2)}, {round(q95,2)}] +/- {round(u50,2)-round(q50,2)}')) %>% spread(group, summary)

OF_distance <- sum.stats.noage.OF_distance.quantile %>% unite(col='group', Mouse_strain,Sex) %>% group_by(group) %>% summarise(summary = glue('{round(q50,2)} [{round(q5,2)}, {round(q95,2)}] +/- {round(u50,2)-round(q50,2)}')) %>% spread(group, summary)

FST_imm<-sum.stats.noage.FST_immobility.quantile %>% unite(col='group', Mouse_strain,Sex) %>% group_by(group) %>% summarise(summary = glue('{round(q50,2)} [{round(q5,2)}, {round(q95,2)}] +/- {round(u50,2)-round(q50,2)}')) %>% spread(group, summary)

EPM_open <- sum.stats.noage.EPM_time_open.quantile %>% unite(col='group', Mouse_strain,Sex) %>% group_by(group) %>% summarise(summary = glue('{round(q50,2)} [{round(q5,2)}, {round(q95,2)}] +/- {round(u50,2)-round(q50,2)}')) %>% spread(group, summary)

#EPM_closed<-sum.stats.noage.EPM_time_closed.quantile %>% unite(col='group', Mouse_strain,Sex) %>% group_by(group) %>% summarise(summary = glue('{round(q50,2)} [{round(q5,2)}, {round(q95,2)}] +/- {round(u50,2)-round(q50,2)}')) %>% spread(group, summary)

bind_rows(OF_time_center,OF_distance,FST_imm,EPM_open) %>% rownames_to_column('Experiment') %>% mutate(Experiment = c('OF: Time spent in center (%)','OF: Distance travelled (m/min)','FST: Immobility time (%)','EPM: Time spent in open arms (%)')) %>% kable("pipe", col.names = gsub("[_]", " ", names(.)))
```

| Experiment                       | BALB/cJ Female                   | BALB/cJ Male                     | C57BL/6J Female                  | C57BL/6J Male                    | C57BL/6N Female                  | C57BL/6N Male                    | DBA/2J Female                | DBA/2J Male                      |
|:---------------------------------|:---------------------------------|:---------------------------------|:---------------------------------|:---------------------------------|:---------------------------------|:---------------------------------|:-----------------------------|:---------------------------------|
| OF: Time spent in center (%)     | 4.98 \[3.56, 6.46\] +/- 13.7     | 6.59 \[5.93, 7.43\] +/- 10.2     | 11.87 \[11.27, 12.51\] +/- 12.04 | 12.15 \[11.85, 12.41\] +/- 11.88 | 14.43 \[12.36, 16.51\] +/- 10.81 | 19.17 \[17.99, 20.37\] +/- 15.3  | 5.04 \[3.96, 6.03\] +/- 5.37 | 12.19 \[11.38, 13.03\] +/- 11.54 |
| OF: Distance travelled (m/min)   | 1.74 \[1.59, 1.88\] +/- 1.15     | 2.61 \[2.49, 2.74\] +/- 2.36     | 2.48 \[2.41, 2.56\] +/- 2.01     | 2.8 \[2.75, 2.84\] +/- 2.17      | 1.68 \[1.56, 1.79\] +/- 1.06     | 3.27 \[2.91, 3.65\] +/- 2.85     | 1.59 \[1.47, 1.69\] +/- 0.97 | 2.95 \[2.84, 3.06\] +/- 2.45     |
| FST: Immobility time (%)         | 48.24 \[46.22, 50.43\] +/- 19.81 | 47.9 \[46.2, 49.5\] +/- 20.79    | 34.71 \[32.66, 36.98\] +/- 21.88 | 45.65 \[44.46, 46.97\] +/- 28.42 | 36.47 \[30.72, 42.85\] +/- 17.26 | 33.55 \[31.27, 35.98\] +/- 19.15 |                              | 22.51 \[19.69, 25.18\] +/- 12.61 |
| EPM: Time spent in open arms (%) | 17.62 \[13.16, 21.86\] +/- 28.28 | 35.95 \[34.08, 37.84\] +/- 36.49 | 19.49 \[18.39, 20.53\] +/- 13.59 | 16.92 \[16.12, 17.65\] +/- 20.78 | 3.17 \[2.94, 3.45\] +/- 0.66     | 11.09 \[9.84, 12.16\] +/- 14.02  |                              | 18.02 \[11.86, 25.29\] +/- 39.21 |

Table 1. Descriptive statistics as a function of strain and sex. Mean
\[5th, 95th intervals\] +/- 1 standard deviation.

![Figure 4](assets/plot/Figure4.png) **Figure 4. Parameter distribution
for remaining experiments.** A) distance travelled in the open field, B)
time spent swimming in the forced swim test, C) time spent in open arms
in the elevated plus maze, D) time spent in the closed arms in the
elevated plus maze.

## Export table effect size

``` r
df <-
  df %>% filter(
    df$Sex != 'Mixed sex',
    df$Mouse_strain != 'Other',
    !df$Age_in_months %in% c('Mixed age', 'Unknown')
  )

exp.type.short <- 'OF_time_center'
OF_time_center <-
  read_tsv(glue('assets/table/effect_size_{exp.type.short}.tsv.gz'),
           col_types = cols()) %>% 
          summarise(sex = glue('{round(quantile(sex,0.5),2)} [{round(quantile(sex,0.05),2)},{round(quantile(sex,0.95),2)}]'), 
                    age = glue('{round(quantile(age,0.5),2)} [{round(quantile(age,0.05),2)},{round(quantile(age,0.95),2)}]'),
                    BALB = glue('{round(quantile(BALB,0.5),2)} [{round(quantile(BALB,0.05),2)},{round(quantile(BALB,0.95),2)}]'), 
                    C57BL = glue('{round(quantile(C57BL,0.5),2)} [{round(quantile(C57BL,0.05),2)},{round(quantile(C57BL,0.95),2)}]'), 
                    DBA = glue('{round(quantile(DBA,0.5),2)} [{round(quantile(DBA,0.05),2)},{round(quantile(DBA,0.95),2)}]'))

exp.type.short <- 'OF_distance'
OF_distance <-
  read_tsv(glue('assets/table/effect_size_{exp.type.short}.tsv.gz'),
           col_types = cols()) %>% 
          summarise(sex = glue('{round(quantile(sex,0.5),2)} [{round(quantile(sex,0.05),2)},{round(quantile(sex,0.95),2)}]'), 
                    age = glue('{round(quantile(age,0.5),2)} [{round(quantile(age,0.05),2)},{round(quantile(age,0.95),2)}]'),
                    BALB = glue('{round(quantile(BALB,0.5),2)} [{round(quantile(BALB,0.05),2)},{round(quantile(BALB,0.95),2)}]'), 
                    C57BL = glue('{round(quantile(C57BL,0.5),2)} [{round(quantile(C57BL,0.05),2)},{round(quantile(C57BL,0.95),2)}]'), 
                    DBA = glue('{round(quantile(DBA,0.5),2)} [{round(quantile(DBA,0.05),2)},{round(quantile(DBA,0.95),2)}]'))


exp.type.short <- 'FST_immobility'
FST_imm <-
  read_tsv(glue('assets/table/effect_size_{exp.type.short}.tsv.gz'),
           col_types = cols()) %>% 
          summarise(sex = glue('{round(quantile(sex,0.5),2)} [{round(quantile(sex,0.05),2)},{round(quantile(sex,0.95),2)}]'), 
                    age = glue('{round(quantile(age,0.5),2)} [{round(quantile(age,0.05),2)},{round(quantile(age,0.95),2)}]'),
                    BALB = glue('{round(quantile(BALB,0.5),2)} [{round(quantile(BALB,0.05),2)},{round(quantile(BALB,0.95),2)}]'), 
                    C57BL = glue('{round(quantile(C57BL,0.5),2)} [{round(quantile(C57BL,0.05),2)},{round(quantile(C57BL,0.95),2)}]'), 
                    DBA = glue('{round(quantile(DBA,0.5),2)} [{round(quantile(DBA,0.05),2)},{round(quantile(DBA,0.95),2)}]'))


exp.type.short <- 'EPM_time_open'
EPM_open <-
  read_tsv(glue('assets/table/effect_size_{exp.type.short}.tsv.gz'),
           col_types = cols()) %>% 
          summarise(sex = glue('{round(quantile(sex,0.5),2)} [{round(quantile(sex,0.05),2)},{round(quantile(sex,0.95),2)}]'), 
                    age = glue('{round(quantile(age,0.5),2)} [{round(quantile(age,0.05),2)},{round(quantile(age,0.95),2)}]'),
                    BALB = glue('{round(quantile(BALB,0.5),2)} [{round(quantile(BALB,0.05),2)},{round(quantile(BALB,0.95),2)}]'), 
                    C57BL = glue('{round(quantile(C57BL,0.5),2)} [{round(quantile(C57BL,0.05),2)},{round(quantile(C57BL,0.95),2)}]'), 
                    DBA = glue('{round(quantile(DBA,0.5),2)} [{round(quantile(DBA,0.05),2)},{round(quantile(DBA,0.95),2)}]'))


exp.type.short <- 'EPM_time_closed'
EPM_closed <-
  read_tsv(glue('assets/table/effect_size_{exp.type.short}.tsv.gz'),
           col_types = cols()) %>% 
          summarise(sex = glue('{round(quantile(sex,0.5),2)} [{round(quantile(sex,0.05),2)},{round(quantile(sex,0.95),2)}]'), 
                    age = glue('{round(quantile(age,0.5),2)} [{round(quantile(age,0.05),2)},{round(quantile(age,0.95),2)}]'),
                    BALB = glue('{round(quantile(BALB,0.5),2)} [{round(quantile(BALB,0.05),2)},{round(quantile(BALB,0.95),2)}]'), 
                    C57BL = glue('{round(quantile(C57BL,0.5),2)} [{round(quantile(C57BL,0.05),2)},{round(quantile(C57BL,0.95),2)}]'), 
                    DBA = glue('{round(quantile(DBA,0.5),2)} [{round(quantile(DBA,0.05),2)},{round(quantile(DBA,0.95),2)}]'))



bind_rows(OF_time_center,OF_distance,FST_imm,EPM_open,EPM_closed) %>% rownames_to_column('Experiment') %>% mutate(Experiment = c('OF: Time spent in center (%)','OF: Distance travelled (m/min)','FST: Immobility time (%)','EPM: Time spent in open arms (%)','EPM: Time spent in closed arms (%)')) %>% 
  kable("pipe", col.names = c('Experiment','Female > Male','Old > Young','C57BL/6j > BALB/cJ','C57BL/6j > C57BL/6n', 'C57BL/6j > DBA/2J'))
```

| Experiment                         | Female &gt; Male      | Old &gt; Young        | C57BL/6j &gt; BALB/cJ | C57BL/6j &gt; C57BL/6n | C57BL/6j &gt; DBA/2J  |
|:-----------------------------------|:----------------------|:----------------------|:----------------------|:-----------------------|:----------------------|
| OF: Time spent in center (%)       | -0.11 \[-0.15,-0.07\] | 0.34 \[0.28,0.39\]    | -0.24 \[-0.29,-0.19\] | 0.06 \[0.03,0.09\]     | 0.09 \[0.05,0.14\]    |
| OF: Distance travelled (m/min)     | -0.24 \[-0.26,-0.21\] | 0.09 \[0.06,0.13\]    | -0.02 \[-0.06,0.04\]  | 0.01 \[-0.02,0.03\]    | 0.05 \[0.02,0.09\]    |
| FST: Immobility time (%)           | -0.22 \[-0.28,-0.16\] | -0.12 \[-0.16,-0.08\] | 0.19 \[0.12,0.25\]    | 0.3 \[0.25,0.34\]      | -0.04 \[-0.09,0.01\]  |
| EPM: Time spent in open arms (%)   | -0.08 \[-0.13,-0.04\] | -0.49 \[-0.55,-0.42\] | 0.13 \[0.1,0.15\]     | 0.01 \[-0.1,0.1\]      | -0.12 \[-0.17,-0.07\] |
| EPM: Time spent in closed arms (%) | 0.14 \[0.09,0.18\]    | 0.4 \[0.34,0.45\]     | -0.25 \[-0.28,-0.22\] | 0.05 \[-0.06,0.16\]    | 0.15 \[0.1,0.19\]     |

**Table 2.** Effect size for main effects of Sex, Age, and Strain.
Values are Cohen’s d with \[5th, 95th intervals\].

## Make the same analysis for the other parameters

Export the figures for the supplementary material.

### Analysis for Open Field: Distance travelled

``` r
df <-
  df %>% filter(
    df$Sex != 'Mixed sex',
    df$Mouse_strain != 'Other',
    !df$Age_in_months %in% c('Mixed age', 'Unknown')
  )

exp.type <- 'OF: Distance travelled (m/min)'
exp.type.label <- "Open Field: \n Distance travelled (m/min)"
exp.type.short <- 'OF_distance'

df.sub <- df %>% filter(Experiment.parameter == exp.type) %>%
  mutate_if(is.factor, fct_drop)

sum.stats <-
  read_tsv(glue('assets/table/summary_stat_{exp.type.short}.tsv.gz'),
           col_types = cols()) %>%
  mutate(Sex = factor(Sex, levels = c("Female", "Male"))) %>%
  mutate(Mouse_strain = factor(
    Mouse_strain,
    levels = c("C57BL/6J",
               "BALB/cJ",
               "C57BL/6N",
               "DBA/2J")
  )) %>%
  mutate(Age_in_months = factor(
    Age_in_months,
    levels = c('0-2',
               '2-4',
               '4-6',
               '6-8',
               '8-10',
               '>10')
  ))

sum.stats.noage <-
  read_tsv(glue('assets/table/summary_stat_noage_{exp.type.short}.tsv.gz'),
           col_types = cols()) %>%
  mutate(Sex = factor(Sex, levels = c("Female", "Male"))) %>%
  mutate(Mouse_strain = factor(
    Mouse_strain,
    levels = c("C57BL/6J",
               "BALB/cJ",
               "C57BL/6N",
               "DBA/2J")
  )) 
  
  effect.size <-
  read_tsv(glue('assets/table/effect_size_{exp.type.short}.tsv.gz'),
           col_types = cols())
effect.size.cov <-
  read_tsv(glue('assets/table/effect_size_cov_{exp.type.short}.tsv.gz'),
           col_types = cols())


figs2 <-
  plot_summary(exp.type,
               df.sub,
               sum.stats,
               sum.stats.noage,
               effect.size,
               exp.type.label)
ggsave(
  'assets/plot/Figure_S2.svg',
  plot = figs2,
  device = 'svg',
  width = 180,
  height = 85,
  units = 'mm',
  dpi = 300
)
ggsave(
  'assets/plot/Figure_S2.png',
  plot = figs2,
  device = 'png',
  width = 180,
  height = 85,
  units = 'mm',
  dpi = 300
)


covariates<-c('Test_duration_total_min','Luminosity_lux', 'Light.dark_cycle')
df.sub.covariates <- df.sub[,names(df.sub)%in% covariates] %>% complete.cases() %>% df.sub[.,]  %>% mutate_if(is.factor,fct_drop)
  
fig_tmp<-df.sub.covariates %>% 
          group_by(Sex, Age_in_months,Mouse_strain) %>% 
          summarise(Count=length(Sample_size),.groups = 'drop') %>% 
          ggballoonplot(x = "Mouse_strain", y = "Age_in_months",
          size = "Count", fill = "Count",
          facet.by = c("Sex"),
          font.main = c(6,"bold.italic", "black"),
          font.x = c(6, "plain", "black"),
          font.y = c(6, "plain", "black"),
          font.legend= c(6, "plain", "black"),
          font.submain= c(6, "plain", "black"),
          font.caption= c(6, "plain", "black"),
          font.tickslab= c(6, "plain", "black"),
          font.xtickslab= c(6, "plain", "black"),
          font.ytickslab= c(6, "plain", "black"),
          xlab = '',
          ylab = '',
          ggtheme = theme_bw())+ 
          scale_fill_gradientn(colors = pal) 

ggsave('assets/plot/misc/Figure_OF_distance_summary.svg', plot = fig_tmp, device = 'svg', width=90,height = 85,units = 'mm',dpi = 300) 
ggsave('assets/plot/misc/Figure_OF_distance_summary.png',  plot = fig_tmp, device = 'png', width=90,height = 85,units = 'mm',dpi = 300)
```

![Figure S2](assets/plot/Figure_S2.png)

### Analysis for Forced Swim test: Immobility time

``` r
df <-
  df %>% filter(
    df$Sex != 'Mixed sex',
    df$Mouse_strain != 'Other',
    !df$Age_in_months %in% c('Mixed age', 'Unknown')
  )

exp.type <- 'FST: Immobility time (%)'
exp.type.label <- "Forced Swim Test: \n Immobility time (%)"
exp.type.short <- 'FST_immobility'

df.sub <- df %>% filter(Experiment.parameter == exp.type) %>%
  mutate_if(is.factor, fct_drop)

sum.stats <-
  read_tsv(glue('assets/table/summary_stat_{exp.type.short}.tsv.gz'),
           col_types = cols()) %>%
  mutate(Sex = factor(Sex, levels = c("Female", "Male"))) %>%
  mutate(Mouse_strain = factor(
    Mouse_strain,
    levels = c("C57BL/6J",
               "BALB/cJ",
               "C57BL/6N",
               "DBA/2J")
  )) %>%
  mutate(Age_in_months = factor(
    Age_in_months,
    levels = c('0-2',
               '2-4',
               '4-6',
               '6-8',
               '8-10',
               '>10')
  ))

sum.stats.noage <-
  read_tsv(glue('assets/table/summary_stat_noage_{exp.type.short}.tsv.gz'),
           col_types = cols()) %>%
  mutate(Sex = factor(Sex, levels = c("Female", "Male"))) %>%
  mutate(Mouse_strain = factor(
    Mouse_strain,
    levels = c("C57BL/6J",
               "BALB/cJ",
               "C57BL/6N",
               "DBA/2J")
  )) 
  
  effect.size <-
  read_tsv(glue('assets/table/effect_size_{exp.type.short}.tsv.gz'),
           col_types = cols())
effect.size.cov <-
  read_tsv(glue('assets/table/effect_size_cov_{exp.type.short}.tsv.gz'),
           col_types = cols())


figs3 <-
  plot_summary(exp.type,
               df.sub,
               sum.stats,
               sum.stats.noage,
               effect.size,
               exp.type.label)
ggsave(
  'assets/plot/Figure_S3.svg',
  plot = figs3,
  device = 'svg',
  width = 180,
  height = 85,
  units = 'mm',
  dpi = 300
)
ggsave(
  'assets/plot/Figure_S3.png',
  plot = figs3,
  device = 'png',
  width = 180,
  height = 85,
  units = 'mm',
  dpi = 300
)

covariates<-c('Test_duration_total_min', 'Water_temperature_celsius', 'Light.dark_cycle')
df.sub.covariates <- df.sub[,names(df.sub)%in% covariates] %>% complete.cases() %>% df.sub[.,]  %>% mutate_if(is.factor,fct_drop)
  
fig_tmp<-df.sub.covariates %>% 
          group_by(Sex, Age_in_months,Mouse_strain) %>% 
          summarise(Count=length(Sample_size),.groups = 'drop') %>% 
          ggballoonplot(x = "Mouse_strain", y = "Age_in_months",
          size = "Count", fill = "Count",
          facet.by = c("Sex"),
          font.main = c(6,"bold.italic", "black"),
          font.x = c(6, "plain", "black"),
          font.y = c(6, "plain", "black"),
          font.legend= c(6, "plain", "black"),
          font.submain= c(6, "plain", "black"),
          font.caption= c(6, "plain", "black"),
          font.tickslab= c(6, "plain", "black"),
          font.xtickslab= c(6, "plain", "black"),
          font.ytickslab= c(6, "plain", "black"),
          xlab = '',
          ylab = '',
          ggtheme = theme_bw())+ 
          scale_fill_gradientn(colors = pal) 

ggsave('assets/plot/misc/Figure_FST_imm_summary.svg', plot = fig_tmp, device = 'svg', width=90,height = 85,units = 'mm',dpi = 300) 
ggsave('assets/plot/misc/Figure_FST_imm_summary.png',  plot = fig_tmp, device = 'png', width=90,height = 85,units = 'mm',dpi = 300)
```

![Figure S3](assets/plot/Figure_S3.png)

### Analysis for EPM: Time spent in open arms

``` r
df <-
  df %>% filter(
    df$Sex != 'Mixed sex',
    df$Mouse_strain != 'Other',
    !df$Age_in_months %in% c('Mixed age', 'Unknown')
  )

exp.type <- 'EPM: Time spent in open arms (%)'
exp.type.label <- "Elevatez Pus Maze: \n Time spent in open arms (%)"
exp.type.short <- 'EPM_time_open'

df.sub <- df %>% filter(Experiment.parameter == exp.type) %>%
  mutate_if(is.factor, fct_drop)

sum.stats <-
  read_tsv(glue('assets/table/summary_stat_{exp.type.short}.tsv.gz'),
           col_types = cols()) %>%
  mutate(Sex = factor(Sex, levels = c("Female", "Male"))) %>%
  mutate(Mouse_strain = factor(
    Mouse_strain,
    levels = c("C57BL/6J",
               "BALB/cJ",
               "C57BL/6N",
               "DBA/2J")
  )) %>%
  mutate(Age_in_months = factor(
    Age_in_months,
    levels = c('0-2',
               '2-4',
               '4-6',
               '6-8',
               '8-10',
               '>10')
  ))

sum.stats.noage <-
  read_tsv(glue('assets/table/summary_stat_noage_{exp.type.short}.tsv.gz'),
           col_types = cols()) %>%
  mutate(Sex = factor(Sex, levels = c("Female", "Male"))) %>%
  mutate(Mouse_strain = factor(
    Mouse_strain,
    levels = c("C57BL/6J",
               "BALB/cJ",
               "C57BL/6N",
               "DBA/2J")
  )) 
  
  effect.size <-
  read_tsv(glue('assets/table/effect_size_{exp.type.short}.tsv.gz'),
           col_types = cols())
effect.size.cov <-
  read_tsv(glue('assets/table/effect_size_cov_{exp.type.short}.tsv.gz'),
           col_types = cols())


figs4 <-
  plot_summary(exp.type,
               df.sub,
               sum.stats,
               sum.stats.noage,
               effect.size,
               exp.type.label)
ggsave(
  'assets/plot/Figure_S4.svg',
  plot = figs4,
  device = 'svg',
  width = 180,
  height = 85,
  units = 'mm',
  dpi = 300
)
ggsave(
  'assets/plot/Figure_S4.png',
  plot = figs4,
  device = 'png',
  width = 180,
  height = 85,
  units = 'mm',
  dpi = 300
)

covariates<-c('Test_duration_total_min','Luminosity_lux', 'Light.dark_cycle')
df.sub.covariates <- df.sub[,names(df.sub)%in% covariates] %>% complete.cases() %>% df.sub[.,]  %>% mutate_if(is.factor,fct_drop)
  
fig_tmp<-df.sub.covariates %>% 
          group_by(Sex, Age_in_months,Mouse_strain) %>% 
          summarise(Count=length(Sample_size),.groups = 'drop') %>% 
          ggballoonplot(x = "Mouse_strain", y = "Age_in_months",
          size = "Count", fill = "Count",
          facet.by = c("Sex"),
          font.main = c(6,"bold.italic", "black"),
          font.x = c(6, "plain", "black"),
          font.y = c(6, "plain", "black"),
          font.legend= c(6, "plain", "black"),
          font.submain= c(6, "plain", "black"),
          font.caption= c(6, "plain", "black"),
          font.tickslab= c(6, "plain", "black"),
          font.xtickslab= c(6, "plain", "black"),
          font.ytickslab= c(6, "plain", "black"),
          xlab = '',
          ylab = '',
          ggtheme = theme_bw())+ 
          scale_fill_gradientn(colors = pal) 

ggsave('assets/plot/misc/Figure_EPM_open_summary.svg', plot = fig_tmp, device = 'svg', width=90,height = 85,units = 'mm',dpi = 300) 
ggsave('assets/plot/misc/Figure_EPM_open_summary.png',  plot = fig_tmp, device = 'png', width=90,height = 85,units = 'mm',dpi = 300)
```

![Figure S4](assets/plot/Figure_S4.png)

### Analysis for EPM: Time spent in closed arms

Currently not running due to bug in database.

``` r
df <-
  df %>% filter(
    df$Sex != 'Mixed sex',
    df$Mouse_strain != 'Other',
    !df$Age_in_months %in% c('Mixed age', 'Unknown')
  )

exp.type <- 'EPM: Time spent in closed arms (%)'
exp.type.label <- "Elevatez Pus Maze: \n Time spent in closed arms (%)"
exp.type.short <- 'EPM_time_closed'

df.sub <- df %>% filter(Experiment.parameter == exp.type) %>%
  mutate_if(is.factor, fct_drop)

sum.stats <-
  read_tsv(glue('assets/table/summary_stat_{exp.type.short}.tsv.gz'),
           col_types = cols()) %>%
  mutate(Sex = factor(Sex, levels = c("Female", "Male"))) %>%
  mutate(Mouse_strain = factor(
    Mouse_strain,
    levels = c("C57BL/6J",
               "BALB/cJ",
               "C57BL/6N",
               "DBA/2J")
  )) %>%
  mutate(Age_in_months = factor(
    Age_in_months,
    levels = c('0-2',
               '2-4',
               '4-6',
               '6-8',
               '8-10',
               '>10')
  ))

sum.stats.noage <-
  read_tsv(glue('assets/table/summary_stat_noage_{exp.type.short}.tsv.gz'),
           col_types = cols()) %>%
  mutate(Sex = factor(Sex, levels = c("Female", "Male"))) %>%
  mutate(Mouse_strain = factor(
    Mouse_strain,
    levels = c("C57BL/6J",
               "BALB/cJ",
               "C57BL/6N",
               "DBA/2J")
  )) 
  
  effect.size <-
  read_tsv(glue('assets/table/effect_size_{exp.type.short}.tsv.gz'),
           col_types = cols())
effect.size.cov <-
  read_tsv(glue('assets/table/effect_size_cov_{exp.type.short}.tsv.gz'),
           col_types = cols())


figs5 <-
  plot_summary(exp.type,
               df.sub,
               sum.stats,
               sum.stats.noage,
               effect.size,
               exp.type.label)
ggsave(
  'assets/plot/Figure_S5.svg',
  plot = figs5,
  device = 'svg',
  width = 180,
  height = 85,
  units = 'mm',
  dpi = 300
)
ggsave(
  'assets/plot/Figure_S5.png',
  plot = figs5,
  device = 'png',
  width = 180,
  height = 85,
  units = 'mm',
  dpi = 300
)

covariates<-c('Test_duration_total_min','Luminosity_lux', 'Light.dark_cycle')
df.sub.covariates <- df.sub[,names(df.sub)%in% covariates] %>% complete.cases() %>% df.sub[.,]  %>% mutate_if(is.factor,fct_drop)
  
fig_tmp<-df.sub.covariates %>% 
          group_by(Sex, Age_in_months,Mouse_strain) %>% 
          summarise(Count=length(Sample_size),.groups = 'drop') %>% 
          ggballoonplot(x = "Mouse_strain", y = "Age_in_months",
          size = "Count", fill = "Count",
          facet.by = c("Sex"),
          font.main = c(6,"bold.italic", "black"),
          font.x = c(6, "plain", "black"),
          font.y = c(6, "plain", "black"),
          font.legend= c(6, "plain", "black"),
          font.submain= c(6, "plain", "black"),
          font.caption= c(6, "plain", "black"),
          font.tickslab= c(6, "plain", "black"),
          font.xtickslab= c(6, "plain", "black"),
          font.ytickslab= c(6, "plain", "black"),
          xlab = '',
          ylab = '',
          ggtheme = theme_bw())+ 
          scale_fill_gradientn(colors = pal) 

ggsave('assets/plot/misc/Figure_EPM_close_summary.svg', plot = fig_tmp, device = 'svg', width=90,height = 85,units = 'mm',dpi = 300) 
ggsave('assets/plot/misc/Figure_EPM_close_summary.png',  plot = fig_tmp, device = 'png', width=90,height = 85,units = 'mm',dpi = 300)
```

## Test for effects against our hypothesis.

``` r
df <-
  df %>% filter(
    df$Sex != 'Mixed sex',
    df$Mouse_strain != 'Other',
    !df$Age_in_months %in% c('Mixed age', 'Unknown')
  )

exp.type <- 'OF: Time spent in center (%)'

df.sub <- df %>% filter(Experiment.parameter == exp.type) %>%
  filter(Mouse_strain == 'C57BL/6J') %>% 
  mutate_if(is.factor, fct_drop)

contrast<-'Female > Male in C57BL/6J' 
sex.contrast<-c(1,-1)
d<-c()

for (i in 1:permutation){  
df.syn <- synth_gen(df.sub) 
d[i] <- lm(Synthetic ~ Sex, df.syn) %>%  contrast_2_d('Sex',sex.contrast)}

H1 <- glue('{round(quantile(d, 0.5),2)} +/- [{round(quantile(d, 0.05),2)},  {round(quantile(d, 0.95),2)}]')

print(glue('{contrast} Cohen d is {H1}'))
```

    ## Female > Male in C57BL/6J Cohen d is -0.01 +/- [-0.07,  0.03]

H1: M &lt; F, C57BL/6 (ID\_MGI: 3028467), time spent in open field
\[%\], 0.20 &lt; d &lt; 0.25 (Fritz et al., 2017).

``` r
df <-
  df %>% filter(
    df$Sex != 'Mixed sex',
    df$Mouse_strain != 'Other',
    !df$Age_in_months %in% c('Mixed age', 'Unknown')
  )

exp.type <- 'OF: Distance travelled (m/min)'

df.sub <- df %>% filter(Experiment.parameter == exp.type) %>%
  filter(Mouse_strain == 'C57BL/6J') %>% 
  mutate_if(is.factor, fct_drop)

contrast<-'Female > Male in C57BL/6J' 
sex.contrast<-c(1,-1)
d<-c()

for (i in 1:permutation){  
df.syn <- synth_gen(df.sub) 
d[i] <- lm(Synthetic ~ Sex, df.syn) %>%  contrast_2_d('Sex',sex.contrast)}

H2 <- glue('{round(quantile(d, 0.5),2)} +/- [{round(quantile(d, 0.05),2)},  {round(quantile(d, 0.95),2)}]')

print(glue('{contrast} Cohen d is {H2}'))
```

    ## Female > Male in C57BL/6J Cohen d is -0.12 +/- [-0.15,  -0.08]

H2: M &lt; F, C57BL/6 (ID\_MGI: 3028467), distance traveled \[m\], 0.5 d
(Tucker et al., 2017).

``` r
df <-
  df %>% filter(
    df$Sex != 'Mixed sex',
    df$Mouse_strain != 'Other',
    !df$Age_in_months %in% c('Mixed age', 'Unknown')
  )

exp.type <- 'OF: Time spent in center (%)'

df.sub <- df %>% filter(Experiment.parameter == exp.type) %>%
  filter(Mouse_strain %in% c('C57BL/6J', 'DBA/2J')) %>% 
  mutate_if(is.factor, fct_drop)

contrast<-'C57BL/6J > DBA/2J' 
strain.contrast<-c(1,-1)
d<-c()

for (i in 1:permutation){  
df.syn <- synth_gen(df.sub) 
d[i] <- lm(Synthetic ~ Mouse_strain, df.syn) %>%  contrast_2_d('Mouse_strain',strain.contrast)}

H3 <- glue('{round(quantile(d, 0.5),2)} +/- [{round(quantile(d, 0.05),2)},  {round(quantile(d, 0.95),2)}]')

print(glue('{contrast} Cohen d is {H3}'))
```

    ## C57BL/6J > DBA/2J Cohen d is 0.09 +/- [0.06,  0.13]

H3: DBA/2 (ID\_MGI: 2684695) &lt; FVB/N (ID\_MGI: 2160001) &lt; C57BL/6
(ID\_MGI: 3028467), time spent in open field \[%\], &lt; 0.2 d (Eltokhi
et al., 2020).

``` r
df <-
  df %>% filter(
    df$Sex != 'Mixed sex',
    df$Mouse_strain != 'Other',
    !df$Age_in_months %in% c('Mixed age', 'Unknown')
  )

exp.type <- 'FST: Immobility time (%)'

df.sub <- df %>% filter(Experiment.parameter == exp.type) %>%
  filter(Mouse_strain == 'C57BL/6J') %>% 
  mutate_if(is.factor, fct_drop)

contrast<-'Female > Male in C57BL/6J' 
sex.contrast<-c(1,-1)
d<-c()

for (i in 1:permutation){  
df.syn <- synth_gen(df.sub) 
d[i] <- lm(Synthetic ~ Sex, df.syn) %>%  contrast_2_d('Sex',sex.contrast)}

H4 <- glue('{round(quantile(d, 0.5),2)} +/- [{round(quantile(d, 0.05),2)},  {round(quantile(d, 0.95),2)}]')

print(glue('{contrast} Cohen d is {H4}'))
```

    ## Female > Male in C57BL/6J Cohen d is -0.3 +/- [-0.36,  -0.23]

H4: M &lt; F, C57BL/6 (ID\_MGI: 3028467), immobility time \[%\], &lt;
0.2 d (Tucker et al., 2017).

``` r
df <-
  df %>% filter(
    df$Sex != 'Mixed sex',
    df$Mouse_strain != 'Other',
    !df$Age_in_months %in% c('Mixed age', 'Unknown')
  )

exp.type <- "FST: Immobility time (%)"

df.sub <- df %>% filter(Experiment.parameter == exp.type) %>%
  filter(Mouse_strain %in% c('C57BL/6J', 'DBA/2J')) %>% 
  mutate_if(is.factor, fct_drop)

contrast<-'DBA/2J > C57BL/6J' 
strain.contrast<-c(-1, 1)
d<-c()

for (i in 1:permutation){  
df.syn <- synth_gen(df.sub) 
d[i] <- lm(Synthetic ~ Mouse_strain, df.syn) %>%  contrast_2_d('Mouse_strain',strain.contrast)}

H5 <- glue('{round(quantile(d, 0.5),2)} +/- [{round(quantile(d, 0.05),2)},  {round(quantile(d, 0.95),2)}]')

print(glue('{contrast} Cohen d is {H5}'))
```

    ## DBA/2J > C57BL/6J Cohen d is -0.3 +/- [-0.34,  -0.26]

H5: C57BL6 (ID\_MGI: 3028467) &lt; DBA/2 (ID\_MGI: 2684695), immobility
time \[%\], 0.3 d (David et al., 2003).

``` r
df <-
  df %>% filter(
    df$Sex != 'Mixed sex',
    df$Mouse_strain != 'Other',
    !df$Age_in_months %in% c('Mixed age', 'Unknown')
  )

exp.type <- 'EPM: Time spent in open arms (%)'

df.sub <- df %>% filter(Experiment.parameter == exp.type) %>%
  filter(Mouse_strain == 'C57BL/6J') %>% 
  mutate_if(is.factor, fct_drop)

contrast<-'Female > Male in C57BL/6J' 
sex.contrast<-c(1,-1)
d<-c()

for (i in 1:permutation){  
df.syn <- synth_gen(df.sub) 
d[i] <- lm(Synthetic ~ Sex, df.syn) %>%  contrast_2_d('Sex',sex.contrast)}

H6 <- glue('{round(quantile(d, 0.5),2)} +/- [{round(quantile(d, 0.05),2)},  {round(quantile(d, 0.95),2)}]')

print(glue('{contrast} Cohen d is {H6}'))
```

    ## Female > Male in C57BL/6J Cohen d is 0.1 +/- [0.04,  0.15]

H6: M &lt; F, C57BL/6 (ID\_MGI: 3028467), time spent in open arms \[%\],
&lt; 0.2 d (Tucker & McCabe, 2017).

``` r
df <-
  df %>% filter(
    df$Sex != 'Mixed sex',
    df$Mouse_strain != 'Other',
    !df$Age_in_months %in% c('Mixed age', 'Unknown')
  )

exp.type <- 'EPM: Time spent in open arms (%)'

df.sub <- df %>% filter(Experiment.parameter == exp.type) %>%
  filter(Mouse_strain == 'C57BL/6J') %>% 
  mutate_if(is.factor, fct_drop)

contrast<-'Male > Female in C57BL/6J' 
sex.contrast<-c(-1,1)
d<-c()

for (i in 1:permutation){  
df.syn <- synth_gen(df.sub) 
d[i] <- lm(Synthetic ~ Sex, df.syn) %>%  contrast_2_d('Sex',sex.contrast)}

H7 <- glue('{round(quantile(d, 0.5),2)} +/- [{round(quantile(d, 0.05),2)},  {round(quantile(d, 0.95),2)}]')

print(glue('{contrast} Cohen d is {H7}'))
```

    ## Male > Female in C57BL/6J Cohen d is -0.1 +/- [-0.15,  -0.04]

H7: M &gt; F, C57BL/6J (ID\_MGI: 3028467), time spent in open arms
\[%\], 0.9 d (AN et al., 2011).

``` r
df <-
  df %>% filter(
    df$Sex != 'Mixed sex',
    df$Mouse_strain != 'Other',
    !df$Age_in_months %in% c('Mixed age', 'Unknown')
  )

exp.type <- 'EPM: Time spent in open arms (%)'

df.sub <- df %>% filter(Experiment.parameter == exp.type) %>%
  filter(Mouse_strain == 'BALB/cJ') %>% 
  mutate_if(is.factor, fct_drop)

contrast<-'Female > Male in BALB/cJ' 
sex.contrast<-c(1,-1)
d<-c()

for (i in 1:permutation){  
df.syn <- synth_gen(df.sub) 
d[i] <- lm(Synthetic ~ Sex, df.syn) %>%  contrast_2_d('Sex',sex.contrast)}

H8 <- glue('{round(quantile(d, 0.5),2)} +/- [{round(quantile(d, 0.05),2)},  {round(quantile(d, 0.95),2)}]')

print(glue('{contrast} Cohen d is {H8}'))
```

    ## Female > Male in BALB/cJ Cohen d is -0.41 +/- [-0.54,  -0.3]

H8: M &lt; F, BALB/cJ (ID\_MGI: 2159737), time spent in open arms \[%\],
0.2 d (AN et al., 2011).

``` r
df <-
  df %>% filter(
    df$Sex != 'Mixed sex',
    df$Mouse_strain != 'Other',
    !df$Age_in_months %in% c('Mixed age', 'Unknown')
  )

exp.type <- "EPM: Time spent in open arms (%)"

df.sub <- df %>% filter(Experiment.parameter == exp.type) %>%
  filter(Mouse_strain %in% c('C57BL/6J', 'BALB/cJ')) %>% 
  mutate_if(is.factor, fct_drop)

contrast<-'BALB/cJ > C57BL/6J' 
strain.contrast<-c(-1, 1)
d<-c()

for (i in 1:permutation){  
df.syn <- synth_gen(df.sub) 
d[i] <- lm(Synthetic ~ Mouse_strain, df.syn) %>%  contrast_2_d('Mouse_strain',strain.contrast)}

H9 <- glue('{round(quantile(d, 0.5),2)} +/- [{round(quantile(d, 0.05),2)},  {round(quantile(d, 0.95),2)}]')

print(glue('{contrast} Cohen d is {H9}'))
```

    ## BALB/cJ > C57BL/6J Cohen d is 0.52 +/- [0.46,  0.59]

H9: C57BL/6 (ID\_MGI: 3028467) &lt; BALB/c (ID\_MGI: 2161072), time
spent in open arms \[%\], 0.8 d (Carola et al., 2002).

Unfortunately, H10 cannot be tested because data for this parameter
wasn’t collected.

``` r
Ht <- tibble(Hypothesis = paste('H', 1:9, sep=''), 'Cohen\'s d' = c(H1, H2, H3, H4, H5, H6, H7, H8, H9))

Ht%>% kable("pipe")
```

| Hypothesis | Cohen’s d                  |
|:-----------|:---------------------------|
| H1         | -0.01 +/- \[-0.07, 0.03\]  |
| H2         | -0.12 +/- \[-0.15, -0.08\] |
| H3         | 0.09 +/- \[0.06, 0.13\]    |
| H4         | -0.3 +/- \[-0.36, -0.23\]  |
| H5         | -0.3 +/- \[-0.34, -0.26\]  |
| H6         | 0.1 +/- \[0.04, 0.15\]     |
| H7         | -0.1 +/- \[-0.15, -0.04\]  |
| H8         | -0.41 +/- \[-0.54, -0.3\]  |
| H9         | 0.52 +/- \[0.46, 0.59\]    |

For reference, here are the original hypothesis, and prior effect size.

### Open-field test

H1: M &lt; F, C57BL/6 (ID\_MGI: 3028467), time spent in open field
\[%\], 0.20 &lt; d &lt; 0.25 (Fritz et al., 2017).  
H2: M &lt; F, C57BL/6 (ID\_MGI: 3028467), distance traveled \[m\], 0.5 d
(Tucker et al., 2017).  
H3: DBA/2 (ID\_MGI: 2684695) &lt; FVB/N (ID\_MGI: 2160001) &lt; C57BL/6
(ID\_MGI: 3028467), time spent in open field \[%\], &lt; 0.2 d (Eltokhi
et al., 2020).

### Forced swim test

H4: M &lt; F, C57BL/6 (ID\_MGI: 3028467), immobility time \[%\], &lt;
0.2 d (Tucker et al., 2017).  
H5: C57BL6 (ID\_MGI: 3028467) &lt; DBA/2 (ID\_MGI: 2684695), immobility
time \[%\], 0.3 d (David et al., 2003).

### Elevated plus maze

H6: M &lt; F, C57BL/6 (ID\_MGI: 3028467), time spent in open arms \[%\],
&lt; 0.2 d (Tucker & McCabe, 2017).  
H7: M &gt; F, C57BL/6J (ID\_MGI: 3028467), time spent in open arms
\[%\], 0.9 d (AN et al., 2011).  
H8: M &lt; F, BALB/cJ (ID\_MGI: 2159737), time spent in open arms \[%\],
0.2 d (AN et al., 2011).  
H9: C57BL/6 (ID\_MGI: 3028467) &lt; BALB/c (ID\_MGI: 2161072), time
spent in open arms \[%\], 0.8 d (Carola et al., 2002).  
H10: C57BL/6 (ID\_MGI: 3028467) &lt; BALB/c (ID\_MGI: 2161072), time
spent in the central area \[%\], 0.7 d (Carola et al., 2002).
